import 'dart:core';

class Student {
  int? id;
  DateTime? created;
  DateTime? updated;
  String? first_name;
  String? last_name;
  String? gender;
  DateTime? date_of_birth;
  String? email;
  String? phone;
  String? aakey;
  int? active_status;
  String? NIN; // Nullable since it can be null
  String? address;
  String? nationality;
  String? passport_number; // Nullable since it can be null
  String? student_unique_id; // Nullable since it can be null

  Student({
    this.id,
    this.created,
    this.updated,
    this.first_name,
    this.last_name,
    this.gender,
    this.date_of_birth,
    this.email,
    this.phone,
    this.aakey,
    this.active_status,
    this.NIN,
    this.address,
    this.nationality,
    this.passport_number,
    this.student_unique_id,
  });

  Student.fromJson(Map<String, dynamic> json) {
    // return Student(
      id = json['id'];
      created = DateTime.parse(json['created']);
      updated = DateTime.parse(json['updated']);
      first_name = json['first_name'];
      last_name = json['last_name'];
      gender = json['gender'];
      date_of_birth = DateTime.parse(json['date_of_birth']);
      email = json['email'];
      phone = json['phone'];
      aakey = json['aakey'] ?? ''; // Default to empty string if null
      active_status =  json['active_status'];
      NIN = json['NIN'];
      address = json['address']; // Default to empty string if null
      nationality = json['nationality'];
      passport_number = json['passport_number'];
      student_unique_id = json['student_unique_id'];
    // );
  }
}
