import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
// import 'models/students.dart';



// //////////

class Student {
  int? id;
  DateTime? created;
  DateTime? updated;
  String? first_name;
  String? last_name;
  String? gender;
  DateTime? date_of_birth;
  String? email;
  String? phone;
  String? aakey;
  int? active_status;
  String? NIN;
  String? address;
  String? nationality;
  String? passport_number;
  String? student_unique_id;

  Student({
    this.id,
    this.created,
    this.updated,
    this.first_name,
    this.last_name,
    this.gender,
    this.date_of_birth,
    this.email,
    this.phone,
    this.aakey,
    this.active_status,
    this.NIN,
    this.address,
    this.nationality,
    this.passport_number,
    this.student_unique_id,
  });

  Student.fromJson(Map<String, dynamic> json) {
    id = json['id'] != null ? int.tryParse(json['id']) : null;
    created = json['created'] != null ? DateTime.parse(json['created']) : null;
    updated = json['updated'] != null ? DateTime.parse(json['updated']) : null;
    first_name = json['first_name'];
    last_name = json['last_name'];
    gender = json['gender'];
    date_of_birth = json['date_of_birth'] != null ? DateTime.parse(json['date_of_birth']) : null;
    email = json['email'];
    phone = json['phone'];
    aakey = json['aakey'] ?? ''; // Provide a fallback value
    active_status = json['active_status'] != null ? int.tryParse(json['active_status']) : null;
    NIN = json['NIN'];
    address = json['address'] ?? ''; // Provide a fallback value
    nationality = json['nationality'];
    passport_number = json['passport_number'];
    student_unique_id = json['student_unique_id'];
  }


}

// /////////

Future<List<Student>> getStudents() async {

var url = Uri.parse("https://copint.org/device_test/get_data.php");
final response = await http.get(url, headers: {"Content-Type": "application/json"});
final List body = json.decode(response.body);
return body.map((e) => Student.fromJson(e)).toList();
}

// /////////

class StudentsPage extends StatefulWidget {
  const StudentsPage({super.key});

  @override
  State<StudentsPage> createState() => _StudentsPageState();

}

class _StudentsPageState extends State<StudentsPage> {

  late Future<List<Student>> studentsFuture;
  final TextEditingController searchController = TextEditingController();
  String searchQuery = "";

 @override
 void initState() {
   super.initState();
   studentsFuture = getStudents();
 }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor:  const Color(0xff18588d),
        title: const Text('All students',
          style: TextStyle(color: Color(0xffffffff)),
        ),
        iconTheme: const IconThemeData(
          color: Color(0xffffffff),
        ),

      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              controller: searchController,
              decoration: const InputDecoration(
                labelText: 'Search',
                suffixIcon: Icon(Icons.search),
              ),
              onChanged: (value) {
                setState(() {
                  searchQuery = value.toLowerCase();
                });
              },
            ),
          ),
          Expanded(
            child: FutureBuilder<List<Student>>(
              future: studentsFuture,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return
                    // //////// pre-loader start
                    const Center(
                    child: SizedBox(
                        width: 50,
                        height: 50,
                        child: CircularProgressIndicator()
                    ),);
                  ///////// pre-loader end
                } else if (snapshot.hasData) {
                  final students = snapshot.data!.where((student) {
                    // Search filter
                    final fullName = '${student.first_name} ${student.last_name}'.toLowerCase();
                    return fullName.contains(searchQuery);
                  }).toList();
                  return buildStudents(students);
                } else if (snapshot.hasError) {
                  return Text('${snapshot.error}');
                } else {
                  return const Text("No students data available");
                }
              },
            ),
          ),
        ],
      ),

      );


  }

//   build student widget
  Widget buildStudents(List<Student> students) {
    return ListView.builder(
      itemCount: students.length,
      itemBuilder: (context, index) {
        final student = students[index];
        String firstname = student.first_name!;
        String lastname = student.last_name!;
        String initials = "${firstname[0]}${lastname[0]}";
        return Container(
          color: Colors.grey.shade100,
          // margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
          padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 5),
          height: 100,
          width: double.maxFinite,
          margin: const EdgeInsets.only(bottom: 20),
          child: ListTile(
            leading: CircleAvatar(
              radius: 30.0,
              backgroundColor: const Color(0xffe0e0e0),
              child: Text(
                  initials.toUpperCase(), style: const TextStyle(fontSize: 20)
              ),
            ),
            title:
            Row(
              children: <Widget>[
                Text(student.first_name!),
                const SizedBox(width: 20),
                Text(student.last_name!),
              ],
            ),
            subtitle: Text(student.gender!),
            trailing: const Icon(Icons.chevron_right),
            onTap: () => {
            // Navigator.push(
            // context,
            // MaterialPageRoute(builder: (context) => StudentDetailsPage(student: student,)),
            // ),
            },
          ),

          // child: Row(
          //   children: [
          //     Text(student.first_name!),
          //   ],
          // ),
        );
      },
    );
  }
// build student widget
}