import 'package:flutter/material.dart';

class MyStepper extends StatefulWidget {
  @override
  _MyStepperState createState() => _MyStepperState();
}

class _MyStepperState extends State<MyStepper> {
  int _currentStep = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Stepper Example'),
      ),
      body: Stepper(
        currentStep: _currentStep,
        onStepContinue: _currentStep < 2 ? () => setState(() => _currentStep += 1) : null,
        onStepCancel: _currentStep > 0 ? () => setState(() => _currentStep -= 1) : null,
        steps: [
          Step(
            title: Text('Step 1'),
            content: Text('Content for Step 1'),
            isActive: _currentStep == 0,
          ),
          Step(
            title: Text('Step 2'),
            content: Text('Content for Step 2'),
            isActive: _currentStep == 1,
          ),
          Step(
            title: Text('Step 3'),
            content: Text('Content for Step 3'),
            isActive: _currentStep == 2,
          ),
        ],
        controlsBuilder: (BuildContext context, ControlsDetails controlsDetails) {
          return _buildStepperControls(controlsDetails);
        },
      ),
    );
  }

  Widget _buildStepperControls(ControlsDetails controlsDetails) {
    return Row(
      children: [
        ElevatedButton(
          onPressed: controlsDetails.onStepContinue,
          child: Text(_currentStep == 2 ? 'Finish' : 'Continue'),
        ),
        SizedBox(width: 16),
        ElevatedButton(
          onPressed: controlsDetails.onStepCancel,
          child: Text('Back'),
        ),
      ],
    );
  }
}
