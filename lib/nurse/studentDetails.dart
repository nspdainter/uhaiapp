import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_expandable_fab/flutter_expandable_fab.dart';
import 'package:hive/hive.dart';
import 'package:uhaiapp/nosql_models/exam.dart';
import 'package:uhaiapp/nurse/edit_allergies.dart';
import 'package:uhaiapp/nurse/edit_chronic_illlness.dart';
import 'package:uhaiapp/nurse/edit_medical_history.dart';
import 'package:uhaiapp/nurse/edit_student_chronic.dart';
import 'package:uhaiapp/nurse/edit_vitals.dart';
import 'package:uhaiapp/nurse/medical_profile.dart';
import 'package:uhaiapp/nurse/mystepper.dart';
// import '../nosql_models/student.dart';
// import '../nosql_models/astudent.dart';
import 'conductExam.dart'; //
import 'package:fl_chart/fl_chart.dart';
import 'package:intl/intl.dart';
import 'edit_family_history.dart';
import 'edit_student_allergies.dart';
import 'edit_student_family_history.dart';
import 'edit_student_medical_history.dart';
import 'edit_vaccination.dart';
import 'examDetails.dart';
import 'localStudents.dart';
import 'allStudents.dart';
import 'vitalsCards/vitalsCards.dart';

// /////////// Get chronic illness

class GetChronic extends StatefulWidget {
  final int studentId;

  GetChronic({required this.studentId});

  @override
  _GetChronicState createState() => _GetChronicState();
}

class _GetChronicState extends State<GetChronic> {
  late Future<List<ChronicCondition>> chronicConditions;

  @override
  void initState() {
    super.initState();
    chronicConditions = fetchChronicConditions(widget.studentId);
  }

  Future<List<ChronicCondition>> fetchChronicConditions(int studentId) async {
    final response = await http.get(
      // https://uhaihealthcare.com/api/v1/school/medical-conditions/student-condition/9/Chronic%20Illness/
      Uri.parse('https://uhaihealthcare.com/api/v1/school/medical-conditions/student-condition/$studentId/Chronic%20Illness'),
    );

    if (response.statusCode == 200) {
      List<dynamic> data = json.decode(response.body);
      return data.map((json) => ChronicCondition.fromJson(json)).toList();
    } else {
      throw Exception('Failed to load chronic conditions. Contact support');
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<ChronicCondition>>(
        future: chronicConditions,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError) {
            return Center(child: Text('Error: ${snapshot.error}'));
          } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
            return Center(child: Text('No chronic conditions added for this student'));
          } else {
            return Wrap(
              spacing: 8.0, // gap between adjacent chips
              runSpacing: 4.0, // gap between lines
              children: snapshot.data!.map((condition) {
                return InputChip(
                  onSelected: (bool value) {},
                  backgroundColor: Color(0xffc8c8c8),
                  label: Text(
                    condition.conditionName,
                    style: TextStyle(color: Color(0xff000000)),
                  ),
                  avatar: Icon(
                    Icons.check,
                    size: 20.0,
                    color: Color(0xff000000),
                  ),
                );
              }).toList(),
            );
          }
        },
      );
  }
}

class ChronicCondition {
  final int id;
  final String conditionName;
  final int conditionType;
  final String created;
  final String? updated;
  final int student;

  ChronicCondition({
    required this.id,
    required this.conditionName,
    required this.conditionType,
    required this.created,
    this.updated,
    required this.student,
  });

  factory ChronicCondition.fromJson(Map<String, dynamic> json) {
    return ChronicCondition(
      id: json['id'],
      conditionName: json['condition_name'],
      conditionType: json['condition_type'],
      created: json['created'],
      updated: json['updated'],
      student: json['student'],
    );
  }
}

// get comments start here
class CommentList extends StatefulWidget {
  final int studentId;

  CommentList({required this.studentId});

  @override
  _CommentListState createState() => _CommentListState();
}

class _CommentListState extends State<CommentList> {
  late Future<String> futureComments;

  @override
  void initState() {
    super.initState();
    futureComments = GetComment(widget.studentId).fetchComments();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<String>(
      future: futureComments,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(child: CircularProgressIndicator());
        } else if (snapshot.hasError) {
          return Center(child: Text('Error: ${snapshot.error}'));
        } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
          return Center(child: Text('No comments found'));
        } else {
          String comments = snapshot.data!;
          print(comments);
          return Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              comments,
              style: TextStyle(fontSize: 16),
            ),
          );
        }
      },
    );
  }
}

class Comment {
  final int id;
  final String? conditionComment;
  final String created;
  final String? updated;
  final int student;
  final int conditionType;

  Comment({
    required this.id,
    required this.conditionComment,
    required this.created,
    this.updated,
    required this.student,
    required this.conditionType,
  });

  factory Comment.fromJson(Map<String, dynamic> json) {
    return Comment(
      id: json['id'],
      conditionComment: json['condition_comment'] ?? '',
      created: json['created'],
      updated: json['updated'],
      student: json['student'],
      conditionType: json['condition_type'],
    );
  }
}

class GetComment {
  final int studentId;

  GetComment(this.studentId);

  Future<String> fetchComments() async {
    final response = await http.get(Uri.parse('https://uhaihealthcare.com/api/v1/school/medical-conditions/student-condition-comments/$studentId/3/'));

    if (response.statusCode == 200) {
      List<dynamic> data = json.decode(response.body);
      return data.map((item) => Comment.fromJson(item).conditionComment).join(' ');
    } else {
      throw Exception('Failed to load comments');
    }
  }
}



// /////////// Get chronic illness ends here

// /////////// Get allergies

class GetAllergies extends StatefulWidget {
  final int studentId;

  GetAllergies({required this.studentId});

  @override
  _GetAllergiesState createState() => _GetAllergiesState();
}

class _GetAllergiesState extends State<GetAllergies> {
  late Future<List<AllergiesCondition>> allergicConditions;

  @override
  void initState() {
    super.initState();
    allergicConditions = fetchAllergiesConditions(widget.studentId);
  }

  Future<List<AllergiesCondition>> fetchAllergiesConditions(int studentId) async {
    final response = await http.get(
      // https://uhaihealthcare.com/api/v1/school/medical-conditions/student-condition/9/Chronic%20Illness/
      Uri.parse('https://uhaihealthcare.com/api/v1/school/medical-conditions/student-condition/$studentId/Allergies'),
    );

    if (response.statusCode == 200) {
      List<dynamic> data = json.decode(response.body);
      return data.map((json) => AllergiesCondition.fromJson(json)).toList();
    } else {
      throw Exception('Failed to load allergies conditions. Contact support');
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<AllergiesCondition>>(
      future: allergicConditions,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(child: CircularProgressIndicator());
        } else if (snapshot.hasError) {
          return Center(child: Text('Error: ${snapshot.error}'));
        } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
          return Center(child: Text('No allergic conditions added for this student'));
        } else {
          return Wrap(
            spacing: 8.0, // gap between adjacent chips
            runSpacing: 4.0, // gap between lines
            children: snapshot.data!.map((condition) {
              return InputChip(
                onSelected: (bool value) {},
                backgroundColor: Color(0xffc8c8c8),
                label: Text(
                  condition.conditionName,
                  style: TextStyle(color: Color(0xff000000)),
                ),
                avatar: Icon(
                  Icons.check,
                  size: 20.0,
                  color: Color(0xff000000),
                ),
              );
            }).toList(),
          );
        }
      },
    );
  }
}

class AllergiesCondition {
  final int id;
  final String conditionName;
  final int conditionType;
  final String created;
  final String? updated;
  final int student;

  AllergiesCondition({
    required this.id,
    required this.conditionName,
    required this.conditionType,
    required this.created,
    this.updated,
    required this.student,
  });

  factory AllergiesCondition.fromJson(Map<String, dynamic> json) {
    return AllergiesCondition(
      id: json['id'],
      conditionName: json['condition_name'],
      conditionType: json['condition_type'],
      created: json['created'],
      updated: json['updated'],
      student: json['student'],
    );
  }
}


// /////////// Get allergies

// /////////// Get MedicalHistory

class GetMedicalHistory extends StatefulWidget {
  final int studentId;

  GetMedicalHistory({required this.studentId});

  @override
  _GetMedicalHistoryState createState() => _GetMedicalHistoryState();
}

class _GetMedicalHistoryState extends State<GetMedicalHistory> {
  late Future<List<MedicalHistoryCondition>> medicalHistoryConditions;

  @override
  void initState() {
    super.initState();
    medicalHistoryConditions = fetchMedicalHistoryConditions(widget.studentId);
  }

  Future<List<MedicalHistoryCondition>> fetchMedicalHistoryConditions(int studentId) async {
    final response = await http.get(
      // https://uhaihealthcare.com/api/v1/school/medical-conditions/student-condition/9/Chronic%20Illness/
      Uri.parse('https://uhaihealthcare.com/api/v1/school/medical-conditions/student-condition/$studentId/Medical%20History'),
    );

    if (response.statusCode == 200) {
      List<dynamic> data = json.decode(response.body);
      return data.map((json) => MedicalHistoryCondition.fromJson(json)).toList();
    } else {
      throw Exception('Failed to load medical history conditions. Contact support');
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<MedicalHistoryCondition>>(
      future: medicalHistoryConditions,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(child: CircularProgressIndicator());
        } else if (snapshot.hasError) {
          return Center(child: Text('Error: ${snapshot.error}'));
        } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
          return Center(child: Text('No medical history conditions added for this student'));
        } else {
          return Wrap(
            spacing: 8.0, // gap between adjacent chips
            runSpacing: 4.0, // gap between lines
            children: snapshot.data!.map((condition) {
              return InputChip(
                onSelected: (bool value) {},
                backgroundColor: Color(0xffc8c8c8),
                label: Text(
                  condition.conditionName,
                  style: TextStyle(color: Color(0xff000000)),
                ),
                avatar: Icon(
                  Icons.check,
                  size: 20.0,
                  color: Color(0xff000000),
                ),
              );
            }).toList(),
          );
        }
      },
    );
  }
}

class MedicalHistoryCondition {
  final int id;
  final String conditionName;
  final int conditionType;
  final String created;
  final String? updated;
  final int student;

  MedicalHistoryCondition({
    required this.id,
    required this.conditionName,
    required this.conditionType,
    required this.created,
    this.updated,
    required this.student,
  });

  factory MedicalHistoryCondition.fromJson(Map<String, dynamic> json) {
    return MedicalHistoryCondition(
      id: json['id'],
      conditionName: json['condition_name'],
      conditionType: json['condition_type'],
      created: json['created'],
      updated: json['updated'],
      student: json['student'],
    );
  }
}

// /////////// Get MedicalHistory

// /////////// Get FamilyHistory

class GetFamilyHistory extends StatefulWidget {
  final int studentId;

  GetFamilyHistory({required this.studentId});

  @override
  _GetFamilyHistoryState createState() => _GetFamilyHistoryState();
}

class _GetFamilyHistoryState extends State<GetFamilyHistory> {
  late Future<List<FamilyHistoryCondition>> familyHistoryConditions;

  @override
  void initState() {
    super.initState();
    familyHistoryConditions = fetchFamilyHistoryConditions(widget.studentId);
  }

  Future<List<FamilyHistoryCondition>> fetchFamilyHistoryConditions(int studentId) async {
    final response = await http.get(
      // https://uhaihealthcare.com/api/v1/school/medical-conditions/student-condition/9/Chronic%20Illness/
      Uri.parse('https://uhaihealthcare.com/api/v1/school/medical-conditions/student-condition/$studentId/Family%20History'),
    );

    if (response.statusCode == 200) {
      List<dynamic> data = json.decode(response.body);
      return data.map((json) => FamilyHistoryCondition.fromJson(json)).toList();
    } else {
      throw Exception('Failed to load family history conditions. Contact support');
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<FamilyHistoryCondition>>(
      future: familyHistoryConditions,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(child: CircularProgressIndicator());
        } else if (snapshot.hasError) {
          return Center(child: Text('Error: ${snapshot.error}'));
        } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
          return Center(child: Text('No family history conditions added for this student'));
        } else {
          return Wrap(
            spacing: 8.0, // gap between adjacent chips
            runSpacing: 4.0, // gap between lines
            children: snapshot.data!.map((condition) {
              return InputChip(
                onSelected: (bool value) {},
                backgroundColor: Color(0xffc8c8c8),
                label: Text(
                  condition.conditionName,
                  style: TextStyle(color: Color(0xff000000)),
                ),
                avatar: Icon(
                  Icons.check,
                  size: 20.0,
                  color: Color(0xff000000),
                ),
              );
            }).toList(),
          );
        }
      },
    );
  }
}

class FamilyHistoryCondition {
  final int id;
  final String conditionName;
  final int conditionType;
  final String created;
  final String? updated;
  final int student;

  FamilyHistoryCondition({
    required this.id,
    required this.conditionName,
    required this.conditionType,
    required this.created,
    this.updated,
    required this.student,
  });

  factory FamilyHistoryCondition.fromJson(Map<String, dynamic> json) {
    return FamilyHistoryCondition(
      id: json['id'],
      conditionName: json['condition_name'],
      conditionType: json['condition_type'],
      created: json['created'],
      updated: json['updated'],
      student: json['student'],
    );
  }
}

// /////////// Get FamilyHistory

// /////////// Get vaccines illness

class GetVaccines extends StatefulWidget {
  final int studentId;

  GetVaccines({required this.studentId});

  @override
  _GetVaccinesState createState() => _GetVaccinesState();
}

class _GetVaccinesState extends State<GetVaccines> {
  late Future<List<Vaccines>> vaccines;

  @override
  void initState() {
    super.initState();
    vaccines = fetchVaccines(widget.studentId);
  }

  Future<List<Vaccines>> fetchVaccines(int studentId) async {
    final response = await http.get(
      // https://uhaihealthcare.com/api/v1/school/medical-conditions/student-condition/9/Chronic%20Illness/
      Uri.parse('https://uhaihealthcare.com/api/v1/school/student-vaccination/$studentId'),
    );

    if (response.statusCode == 200) {
      List<dynamic> data = json.decode(response.body);
      return data.map((json) => Vaccines.fromJson(json)).toList();
    } else {
      throw Exception('Failed to load vaccines. Contact support');
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Vaccines>>(
      future: vaccines,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(child: CircularProgressIndicator());
        } else if (snapshot.hasError) {
          return Center(child: Text('Error: ${snapshot.error}'));
        } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
          return Center(child: Text('No vaccines added for this student'));
        } else {
          return Wrap(
            spacing: 10.0, // gap between adjacent chips
            runSpacing: 10.0, // gap between lines
            children: snapshot.data!.map((vaccine) {
              String theVaccine = vaccine.vaccineName;
              String? theVaccineDate = vaccine.vaccineDate;
              return InputChip(
                onSelected: (bool value) {},
                backgroundColor: Color(0xffc8c8c8),
                label: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(theVaccine, style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Color(0xff000000))),
                    Text(
                        theVaccineDate ?? 'Unknown date',
                        style: TextStyle(fontSize: 12, color: Color(0xff18588d))),
                  ],
                ),

                avatar: Icon(
                  Icons.check,
                  size: 20.0,
                  color: Color(0xff000000),
                ),
              );
            }).toList(),
          );
        }
      },
    );
  }
}

class Vaccines {
  final int id;
  final String vaccineName;
  final int vaccineId;
  final String? vaccineDate;
  final int studentID;

  Vaccines({
    required this.id,
    required this.vaccineName,
    required this.vaccineId,
    required this.vaccineDate,
    required this.studentID,
  });

  factory Vaccines.fromJson(Map<String, dynamic> json) {
    return Vaccines(
      id: json['id'],
      vaccineName: json['vaccine_name'],
      vaccineId: json['vaccine'],
      vaccineDate: json['vaccine_issue_date'] ?? 'Unknown date',
      studentID: json['student'],

    );
  }
}




// /////////// Get vaccines illness



class TemperatureRecord {
  final int id;
  final String date;
  final String temperature;

  TemperatureRecord({required this.id, required this.date, required this.temperature});
}

class TemperatureTable extends StatelessWidget {
  final List<TemperatureRecord> records = [
    TemperatureRecord(id: 1, date: '2024-03-01', temperature: '36.6°C'),
    TemperatureRecord(id: 2, date: '2024-03-02', temperature: '37.0°C'),
    TemperatureRecord(id: 3, date: '2024-03-03', temperature: '36.8°C'),
    TemperatureRecord(id: 4, date: '2024-03-04', temperature: '37.2°C'),
    TemperatureRecord(id: 5, date: '2024-03-05', temperature: '36.5°C'),
  ];

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: DataTable(
        columns: const [
          DataColumn(label: Text('No. (id)')),
          DataColumn(label: Text('Date')),
          DataColumn(label: Text('Temperature')),
        ],
        rows: records.map((record) {
          return DataRow(cells: [
            DataCell(Text(record.id.toString())),
            DataCell(Text(record.date)),
            DataCell(Text(record.temperature)),
          ]);
        }).toList(),
      ),
    );
  }
}

// FAB variable
final scaffoldKey = GlobalKey<ScaffoldMessengerState>();
// FAB variable

class StudentDetailsPage extends StatefulWidget {
  // final Astudent student;
  final Student student;


  const StudentDetailsPage({
    Key? key,
    required this.student,
  }) : super(key: key);

  @override
  _StudentDetailsPageState createState() => _StudentDetailsPageState();
}

class _StudentDetailsPageState extends State<StudentDetailsPage> with SingleTickerProviderStateMixin {

  // vitals initializer start

  final double bmiValue = 31.0;
  final double tempValue = 39.9;
  final double weightValue = 57.3;
  final double glucoseValue = 80;
  final int sp02Value = 97;
  final int pbrmValue = 75;
  final int sysValue = 63;
  final int diaValue = 176;
  final int pulValue = 19;

  // vital initializer end


  late TabController _tabController;

  late Box<Exam> examBox;

  // FAB
  final _key = GlobalKey<ExpandableFabState>();
  // FAB

  bool isExpanded = false; // ExpansionTile state




  @override
  void initState() {
    super.initState();
    examBox = Hive.box<Exam>('exams');
    _tabController = TabController(length: 2, vsync: this);
    // Update the UI when the tab changes
    _tabController.addListener(() {
      // Force widget rebuild on tab change
      if (!mounted) return; // Check if the widget is still in the widget tree
      setState(() {
        // The UI will rebuild, and the FloatingActionButton's visibility will be updated
      });
    });

    //   FAB

    //   FAB

  }

  @override
  void dispose() {
    _tabController.dispose(); // Dispose of the TabController to clean up resources

    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    // final Student student = studentBox.getAt(index) as Student;
    String firstname = widget.student.first_name!;
    String lastname = widget.student.last_name!;
    String initials = "${firstname[0]}${lastname[0]}";

    // /////////
    final List<Exam> studentExams = examBox.values.where((exam) => exam.studentId == widget.student.id).toList();

    return Scaffold(
      appBar: AppBar(
        bottom: TabBar(
          isScrollable: true,
          indicatorColor: const Color(0xff70c05d),
          unselectedLabelColor: const Color(0xffffffff),
          labelColor: const Color(0xff70c05d),
          controller: _tabController,
          tabs: [
            Tab(text: "OVERVIEW"),
            Tab(text: "MEDICAL PROFILE"),
            // Tab(text: "MEDICAL EXAMS"),

          ],
        ),
        backgroundColor: const Color(0xff18588d),
        title: Text(
            widget.student.first_name!,
            style: TextStyle(color: const Color(0xffffffff))
        ),
        iconTheme: IconThemeData(
          color: const Color(0xffffffff),
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        children: [

          SingleChildScrollView(
            child: Column (
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Card(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          padding: EdgeInsets.all(8.0),
                          color: const Color(0xff949494), // Grey colored heading section
                          width: double.infinity, // Makes the container take full width
                          child: Text(
                            'Bio data',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        SizedBox(height: 10,),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              height: 128,
                              width: 128,
                              color: Colors.grey,
                              child: Center(
                                child: Text(
                                  initials.toUpperCase(),
                                  style: TextStyle(
                                    fontSize: 80,
                                    color: const Color(0xff000000),
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(width: 10,),
                            Expanded(
                                child: Padding(
                                  padding: EdgeInsets.all(0.0),
                                  child: ExpansionTile(
                                    initiallyExpanded:true,
                                    collapsedBackgroundColor: const Color(0xff949494),
                                    collapsedTextColor: const Color(0xff000000),
                                    title: Text('Bio Details', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                                    trailing: Icon(Icons.expand_more),
                                    children: <Widget>[
                                      Align(
                                        alignment: Alignment.centerLeft,
                                        child: Padding(
                                          padding: EdgeInsets.all(8.0),
                                          child: Wrap(
                                              alignment: WrapAlignment.start,
                                              spacing: 8.0,
                                              children: [
                                                Padding(
                                                  padding: const EdgeInsets.all(8.0),
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      Padding(
                                                          padding: EdgeInsets.all(0.0),
                                                          child: Wrap(
                                                              spacing: 8.0, // space between cards horizontally
                                                              runSpacing: 8.0, // space between cards vertically
                                                              children: <Widget>[
                                                                Text(
                                                                  widget.student.first_name.toString(),
                                                                  style: TextStyle(
                                                                    fontSize: 18,
                                                                    fontWeight: FontWeight.bold,
                                                                    color: Color(0xff18588d),
                                                                  ),
                                                                ),

                                                                Text(
                                                                  widget.student.last_name.toString(),
                                                                  style: TextStyle(
                                                                    fontSize: 18,
                                                                    fontWeight: FontWeight.bold,
                                                                    color: Color(0xff18588d),
                                                                  ),
                                                                ),

                                                              ]
                                                          )
                                                      ),

                                                      Container(
                                                        padding: const EdgeInsets.only(bottom: 2),
                                                        child: Text(
                                                          widget.student.gender! == "M" ? "MALE" : "FEMALE",
                                                          style: TextStyle(fontWeight: FontWeight.bold),
                                                        ),
                                                      ),
                                                      Table(
                                                        columnWidths: {
                                                          0: FlexColumnWidth(1), // Adjusts the width of the label column
                                                        },
                                                        children: [
                                                          _buildTableRow('Age:', ''),
                                                          _buildTableRow('Ethinicity:', ''),
                                                          _buildTableRow('Religion:', ''),
                                                          _buildTableRow('Class:', ''),
                                                          _buildTableRow('ID:', '${widget.student.id}'),
                                                          _buildTableRow('uhai ID:', ''),
                                                          _buildTableRow('NIN:', ''),
                                                          _buildTableRow('Home Address:', '${widget.student.address}'),
                                                        ],
                                                      ),

                                                    ],
                                                  ),
                                                ),
                                              ]

                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      // Padding(
                                      //   padding: EdgeInsets.all(8.0),
                                      //   child: Align(
                                      //       alignment: Alignment.topLeft,
                                      //       child: Column(
                                      //         crossAxisAlignment: CrossAxisAlignment.start,
                                      //         mainAxisAlignment: MainAxisAlignment.start,
                                      //         children: <Widget>[
                                      //           Text("Comment", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                                      //           SizedBox(height: 15,),
                                      //           // Expanded(
                                      //           //     child: CommentList(studentId: widget.student.id!)
                                      //           // )
                                      //
                                      //           // Text(widget.student.chronicIllnessComment!),
                                      //         ],
                                      //
                                      //       )
                                      //   ),
                                      //
                                      // )

                                    ],
                                  ),
                                ),
                            ),
                            // Expanded(
                            //   child: Padding(
                            //     padding: const EdgeInsets.all(8.0),
                            //     child: Column(
                            //       crossAxisAlignment: CrossAxisAlignment.start,
                            //       children: [
                            //         Padding(
                            //             padding: EdgeInsets.all(0.0),
                            //             child: Wrap(
                            //                 spacing: 8.0, // space between cards horizontally
                            //                 runSpacing: 8.0, // space between cards vertically
                            //                 children: <Widget>[
                            //                   Text(
                            //                     widget.student.first_name.toString(),
                            //                     style: TextStyle(
                            //                       fontSize: 18,
                            //                       fontWeight: FontWeight.bold,
                            //                       color: Color(0xff18588d),
                            //                     ),
                            //                   ),
                            //
                            //                   Text(
                            //                     widget.student.last_name.toString(),
                            //                     style: TextStyle(
                            //                       fontSize: 18,
                            //                       fontWeight: FontWeight.bold,
                            //                       color: Color(0xff18588d),
                            //                     ),
                            //                   ),
                            //
                            //                 ]
                            //            )
                            //         ),
                            //
                            //         Container(
                            //           padding: const EdgeInsets.only(bottom: 2),
                            //           child: Text(
                            //             widget.student.gender! == "M" ? "MALE" : "FEMALE",
                            //             style: TextStyle(fontWeight: FontWeight.bold),
                            //           ),
                            //         ),
                            //         Table(
                            //           columnWidths: {
                            //             0: FlexColumnWidth(1), // Adjusts the width of the label column
                            //           },
                            //           children: [
                            //             _buildTableRow('Age:', ''),
                            //             _buildTableRow('Ethinicity:', ''),
                            //             _buildTableRow('Class:', ''),
                            //             _buildTableRow('ID:', '${widget.student.id}'),
                            //             _buildTableRow('Home Address:', '${widget.student.address}'),
                            //           ],
                            //         ),
                            //
                            //       ],
                            //     ),
                            //   ),
                            // ),
                            // SizedBox(width: 20),
                            // Flexible(
                            //   child: Column(
                            //     crossAxisAlignment: CrossAxisAlignment.end,
                            //     children: [
                            //       BMICard(bmi: bmiValue),
                            //       Card(
                            //         child: Container(
                            //           width: 100,
                            //           height: 60,
                            //           color: const Color(0xffc8c8c8),
                            //           child: Column(
                            //             children: [
                            //               SizedBox(height: 8,),
                            //               Text('Height: (cm)'),
                            //               Text('163.1', style: TextStyle(fontWeight: FontWeight.bold),)
                            //             ],
                            //           ),
                            //         ),
                            //       ),
                            //       Card(
                            //         child: Container(
                            //           width: 100,
                            //           height: 60,
                            //           color: const Color(0xffc8c8c8),
                            //           child: Column(
                            //             children: [
                            //               SizedBox(height: 8,),
                            //               Text('Blood Group:'),
                            //               Text('AB', style: TextStyle(fontWeight: FontWeight.bold),)
                            //             ],
                            //           ),
                            //         ),
                            //       ),
                            //     ],
                            //   ),
                            // ),
                          ],
                        ),
                        // Row(
                        //   crossAxisAlignment: CrossAxisAlignment.start,
                        //   children: [
                        //     Flexible(
                        //       child: Row(
                        //         // crossAxisAlignment: CrossAxisAlignment.end,
                        //         children: [
                        //           BMICard(bmi: bmiValue),
                        //
                        //           Card(
                        //             child: Container(
                        //               width: 100,
                        //               height: 60,
                        //               color: const Color(0xffc8c8c8),
                        //               child: Column(
                        //                 children: [
                        //                   SizedBox(height: 8,),
                        //                   Text('Blood Group:'),
                        //                   Text('AB', style: TextStyle(fontWeight: FontWeight.bold),)
                        //                 ],
                        //               ),
                        //             ),
                        //           ),
                        //         ],
                        //       ),
                        //     ),
                        //   ],
                        // ),
                      ],
                    ),
                  ),
                ),
                // Padding(
                //   padding: const EdgeInsets.all(8.0),
                //   child: Card(
                //     child: Column(
                //       crossAxisAlignment: CrossAxisAlignment.start,
                //       children: [
                //
                //         Container(
                //           padding: EdgeInsets.all(8.0),
                //           color: const Color(0xff949494), // Grey colored heading section
                //           width: double.infinity, // Makes the container take full width
                //           child: Text(
                //             'Last Vitals',
                //             style: TextStyle(
                //               fontWeight: FontWeight.bold,
                //             ),
                //           ),
                //         ),
                //         // SizedBox(height: 40,),
                //         Padding(
                //           padding: const EdgeInsets.all(8.0),
                //           child: Wrap(
                //             spacing: 8.0, // space between cards horizontally
                //             runSpacing: 8.0, // space between cards vertically
                //             children: <Widget>[
                //
                //               TempCard(temp: tempValue),
                //               WeightCard(weight: weightValue),
                //               GlucoseCard(glucose: glucoseValue),
                //
                //               Card(
                //                 child: Container(
                //                   width: 100,
                //                   height: 60,
                //                   color: const Color(0xffc8c8c8),
                //                   child: Column(children: [SizedBox(height: 8,),Text('Height:'),Text('134.1 cm', style: TextStyle(fontWeight: FontWeight.bold),)]),
                //                 ),
                //               ),
                //
                //
                //               // Add more cards as needed
                //             ],
                //           ),
                //         ),
                //
                //       ],
                //     ),
                //   ),
                // ),
                // SizedBox(height: 20,),
                // Padding(
                //   padding: const EdgeInsets.all(8.0),
                //   child: Card(
                //     child: Column(
                //       crossAxisAlignment: CrossAxisAlignment.start,
                //       children: [
                //         Container(
                //           padding: EdgeInsets.all(8.0),
                //           // color: const Color(0xff949494), // Grey colored heading section
                //           width: double.infinity, // Makes the container take full width
                //           child: Text(
                //             'Oxygyen Saturation Level',
                //             style: TextStyle(
                //               fontWeight: FontWeight.bold,
                //             ),
                //           ),
                //         ),
                //
                //         Padding(
                //           padding: const EdgeInsets.all(8.0),
                //           child: Container(
                //             width: double.infinity,
                //             child: Wrap(
                //               spacing: 8.0, // space between cards horizontally
                //               runSpacing: 8.0, // space between cards vertically
                //               children: <Widget>[
                //                 // Add your Card widgets here
                //                 Sp02Card(sp02: sp02Value),
                //                 PbrmCard(pbrm: pbrmValue),
                //
                //                 // Add more cards as needed
                //               ],
                //             ),
                //           ),
                //
                //         ),
                //
                //       ],
                //     ),
                //   ),
                // ),
                // SizedBox(height: 20,),
                // Padding(
                //   padding: const EdgeInsets.all(8.0),
                //   child: Card(
                //     child: Column(
                //       crossAxisAlignment: CrossAxisAlignment.start,
                //       children: [
                //
                //         Container(
                //           padding: EdgeInsets.all(8.0),
                //           // color: const Color(0xff949494), // Grey colored heading section
                //           width: double.infinity, // Makes the container take full width
                //           child: Text(
                //             'Blood Pressure',
                //             style: TextStyle(
                //               fontWeight: FontWeight.bold,
                //             ),
                //           ),
                //         ),
                //         // SizedBox(height: 40,),
                //
                //
                //         Padding(
                //           padding: const EdgeInsets.all(8.0),
                //           child: Wrap(
                //             spacing: 8.0, // space between cards horizontally
                //             runSpacing: 8.0, // space between cards vertically
                //             children: <Widget>[
                //               // Add your Card widgets here
                //
                //               SysCard(sys: sysValue),
                //               DiaCard(dia: diaValue),
                //               PulCard(pul: pulValue),
                //
                //               // Add more cards as needed
                //             ],
                //           ),
                //         ),
                //
                //       ],
                //     ),
                //   ),
                // ),
                SizedBox(height: 20,),


                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: ExpansionTile(
                    initiallyExpanded:true,
                    collapsedBackgroundColor: const Color(0xff949494),
                    collapsedTextColor: const Color(0xff000000),
                    title: Text('Chronic Illnesses', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                    trailing: Icon(Icons.expand_more),
                    children: <Widget>[
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Wrap(
                            alignment: WrapAlignment.start,
                            spacing: 8.0,
                            children: [
                              GetChronic(studentId: widget.student.id!),
                            ]

                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Text("Comment", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                            SizedBox(height: 15,),
                            // Expanded(
                            //     child: CommentList(studentId: widget.student.id!)
                            // )

                            // Text(widget.student.chronicIllnessComment!),
                          ],

                        )
                        ),

                      )

                    ],
                  ),
                ),
                const SizedBox(height: 20,),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: ExpansionTile(
                    initiallyExpanded:true,
                    collapsedBackgroundColor: Color(0xff949494),
                    collapsedTextColor: Color(0xff000000),
                    title: Text('Allergies', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                    trailing: Icon(Icons.expand_more),
                    children: <Widget>[
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Wrap(
                            alignment: WrapAlignment.start,
                            spacing: 8.0,
                            children: [
                              GetAllergies(studentId: widget.student.id!),
                            ]

                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text("Comment", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                                SizedBox(height: 15,),
                                // Text(widget.student.allergiesComment!),
                              ],

                            )
                        ),

                      )
                    ],
                  ),
                ),

                const SizedBox(height: 20,),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: ExpansionTile(
                    initiallyExpanded:true,
                    collapsedBackgroundColor: Color(0xff949494),
                    collapsedTextColor: Color(0xff000000),
                    title: Text('Medical History', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                    trailing: Icon(Icons.expand_more),
                    children: <Widget>[
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Wrap(
                            alignment: WrapAlignment.start,
                            spacing: 8.0,
                            children: [
                              GetMedicalHistory(studentId: widget.student.id!),
                            ]

                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text("Comments", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                                SizedBox(height: 15,),
                                // Text(widget.student.medicalHistoryComment!),
                              ],

                            )
                        ),

                      )

                    ],
                  ),
                ),

              ],),
          ),
          SingleChildScrollView(
            child: Column (
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Card(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          padding: EdgeInsets.all(8.0),
                          color: const Color(0xff949494), // Grey colored heading section
                          width: double.infinity, // Makes the container take full width
                          child: Text(
                            'More on medical profile',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        SizedBox(height: 10,),

                      ],
                    ),
                  ),
                ),

                // SizedBox(height: 20,),

                // SizedBox(height: 20,),


                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: ExpansionTile(
                    initiallyExpanded:true,
                    collapsedBackgroundColor: const Color(0xff949494),
                    collapsedTextColor: const Color(0xff000000),
                    title: Text('Chronic Illnesses', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                    trailing: Icon(Icons.expand_more),
                    children: <Widget>[
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Wrap(
                            alignment: WrapAlignment.start,
                            spacing: 8.0,
                            children: [
                              GetChronic(studentId: widget.student.id!),
                              // get_student_chronic(widget.student.id),
                            ]
                            // widget.student.chronicIllness.map((illness) {
                            //   return InputChip(
                            //     onSelected: (bool value) {},
                            //     backgroundColor: Color(0xffc8c8c8),
                            //     label: Text(
                            //       illness['condition_name'],
                            //       style: TextStyle(color: Color(0xff000000)),
                            //     ),
                            //     avatar: Icon(
                            //       Icons.check,
                            //       size: 20.0,
                            //       color: Color(0xff000000),
                            //     ),
                            //   );
                            // }).toList(),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text("Comment", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                                SizedBox(height: 15,),
                                // Text(widget.student.chronicIllnessComment!),
                              ],

                            )
                        ),

                      )

                    ],
                  ),
                ),
                const SizedBox(height: 20,),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: ExpansionTile(
                    initiallyExpanded:true,
                    collapsedBackgroundColor: Color(0xff949494),
                    collapsedTextColor: Color(0xff000000),
                    title: Text('Allergies', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                    trailing: Icon(Icons.expand_more),
                    children: <Widget>[
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Wrap(
                            alignment: WrapAlignment.start,
                            spacing: 8.0,
                            children: [
                              GetAllergies(studentId: widget.student.id!)
                            ]
                            // widget.student.allergies.map((allergy) {
                            //   return InputChip(
                            //     onSelected: (bool value) {},
                            //     backgroundColor: Color(0xffc8c8c8),
                            //     label: Text(
                            //       allergy['name'],
                            //       style: TextStyle(color: Color(0xff000000)),
                            //     ),
                            //     avatar: Icon(
                            //       Icons.check,
                            //       size: 20.0,
                            //       color: Color(0xff000000),
                            //     ),
                            //   );
                            // }).toList(),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text("Comment", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                                SizedBox(height: 15,),
                                // Text(widget.student.allergiesComment!),
                              ],

                            )
                        ),

                      )
                    ],
                  ),
                ),

                const SizedBox(height: 20,),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: ExpansionTile(
                    initiallyExpanded:true,
                    collapsedBackgroundColor: Color(0xff949494),
                    collapsedTextColor: Color(0xff000000),
                    title: Text('Medical History', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                    trailing: Icon(Icons.expand_more),
                    children: <Widget>[
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Wrap(
                            alignment: WrapAlignment.start,
                            spacing: 8.0,
                            children: [
                              GetMedicalHistory(studentId: widget.student.id!),
                            ]
                            // widget.student.medicalHistory.map((medicalhistory) {
                            //   return InputChip(
                            //     onSelected: (bool value) {},
                            //     backgroundColor: Color(0xffc8c8c8),
                            //     label: Text(
                            //       medicalhistory['condition_name'],
                            //       style: TextStyle(color: Color(0xff000000)),
                            //     ),
                            //     avatar: Icon(
                            //       Icons.check,
                            //       size: 20.0,
                            //       color: Color(0xff000000),
                            //     ),
                            //   );
                            // }).toList(),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text("Comments", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                                SizedBox(height: 15,),
                                // Text(widget.student.medicalHistoryComment!),
                              ],

                            )
                        ),

                      )

                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: ExpansionTile(
                    initiallyExpanded:true,
                    collapsedBackgroundColor: const Color(0xff949494),
                    collapsedTextColor: const Color(0xff000000),
                    title: Text('Family History', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                    trailing: Icon(Icons.expand_more),
                    children: <Widget>[
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Wrap(
                            alignment: WrapAlignment.start,
                            spacing: 8.0,
                            children: [
                              GetFamilyHistory(studentId: widget.student.id!),
                            ]
                            // widget.student.chronicIllness.map((illness) {
                            //   return InputChip(
                            //     onSelected: (bool value) {},
                            //     backgroundColor: Color(0xffc8c8c8),
                            //     label: Text(
                            //       illness['condition_name'],
                            //       style: TextStyle(color: Color(0xff000000)),
                            //     ),
                            //     avatar: Icon(
                            //       Icons.check,
                            //       size: 20.0,
                            //       color: Color(0xff000000),
                            //     ),
                            //   );
                            // }).toList(),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text("Comment", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                                SizedBox(height: 15,),
                                // Text(widget.student.chronicIllnessComment!),
                              ],

                            )
                        ),

                      )

                    ],
                  ),
                ),
                const SizedBox(height: 20,),

                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: ExpansionTile(
                    initiallyExpanded:true,
                    collapsedBackgroundColor: const Color(0xff949494),
                    collapsedTextColor: const Color(0xff000000),
                    title: Text('Vaccination', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                    trailing: Icon(Icons.expand_more),
                    children: <Widget>[
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Wrap(
                              alignment: WrapAlignment.start,
                              spacing: 8.0,
                              children: [
                                GetVaccines(studentId: widget.student.id!),
                              ]
                            // widget.student.chronicIllness.map((illness) {
                            //   return InputChip(
                            //     onSelected: (bool value) {},
                            //     backgroundColor: Color(0xffc8c8c8),
                            //     label: Text(
                            //       illness['condition_name'],
                            //       style: TextStyle(color: Color(0xff000000)),
                            //     ),
                            //     avatar: Icon(
                            //       Icons.check,
                            //       size: 20.0,
                            //       color: Color(0xff000000),
                            //     ),
                            //   );
                            // }).toList(),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text("Comment", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                                SizedBox(height: 15,),
                                // Text(widget.student.chronicIllnessComment!),
                              ],

                            )
                        ),

                      )

                    ],
                  ),
                ),
                const SizedBox(height: 20,),

              ],),
          ),

          // SingleChildScrollView(
          //   child: Column (
          //     children: [
          //
          //       Padding(padding: const EdgeInsets.all(8.0),
          //         child: SizedBox(
          //           // height: 500,
          //           child: Container(
          //             padding: EdgeInsets.all(8.0),
          //             color: const Color(0xff949494), // Grey colored heading section
          //             width: double.infinity, // Makes the container take full width
          //             child: Text(
          //               'Medical Exams',
          //               style: TextStyle(
          //                 fontWeight: FontWeight.bold,
          //               ),
          //             ),
          //           ),
          //         ),),
          //
          //       //   //////list the conducted exams here
          //       Padding(padding: const EdgeInsets.all(8.0),
          //         child: SizedBox(
          //             height: 800,
          //             child:  ListView.builder(
          //                 itemCount: examBox.length,
          //                 itemBuilder: ((context, index){
          //                   final exam = examBox.getAt(index) as Exam;
          //                   print(exam.studentId);
          //
          //                   return Card(
          //                     shape: RoundedRectangleBorder( // Specify the shape of the Card
          //                       borderRadius: BorderRadius
          //                           .zero, // Set borderRadius to zero for sharp corners
          //                     ),
          //                     elevation: 2.0,
          //                     // margin: EdgeInsets.all(8.0),
          //                     child: Column(
          //                       crossAxisAlignment: CrossAxisAlignment.start,
          //                       children: [ // Card Header
          //                         Container(
          //                           padding: EdgeInsets.all(8.0),
          //                           color: const Color(0xffd9d9d9),
          //                           child: Row(
          //                             children: [
          //                               Text(
          //                                 'Student ID:' + exam.studentId,
          //                                 style: TextStyle(
          //                                   fontWeight: FontWeight.bold,
          //                                   // Makes the text bold
          //                                   fontSize: 12.0,
          //                                   // Increase the font size
          //                                   color: Colors
          //                                       .black, // Darker blue color for the text
          //                                 ),
          //                               ),
          //                             ],
          //                           ),
          //                         ),
          //
          //                         Container(
          //                           padding: EdgeInsets.all(8.0),
          //                           child: Column(
          //                             children: [
          //                               ListTile(
          //                                 leading: Column(
          //                                     children: [
          //                                       Text("Exam ID:"),
          //                                       // Text(exam.examId.toString()),
          //                                       Text(exam.key.toString()),
          //                                     ]
          //                                 ),
          //
          //                                 title: Column(
          //                                   children: [
          //                                     Text("Date and time:"),
          //                                     Text(
          //                                         formatDateTime(exam.createdAt)
          //                                             .toString()),
          //                                   ],
          //                                 ),
          //
          //
          //                                 trailing: OutlinedButton(
          //                                   onPressed: () {
          //                                     Navigator.of(context).push(
          //                                       MaterialPageRoute(
          //                                         builder: (context) =>
          //                                             ExamDetails(exam: exam,
          //                                                 student: widget
          //                                                     .student),
          //                                       ),
          //                                     );
          //                                   },
          //                                   child: Text('Details'),
          //                                   style: OutlinedButton.styleFrom(
          //                                     side: BorderSide(
          //                                         color: Color(0xffd3d3d3),
          //                                         width: 2.0),
          //                                     foregroundColor: const Color(
          //                                         0xff000000),
          //
          //                                     shape: RoundedRectangleBorder(
          //                                       borderRadius: BorderRadius
          //                                           .zero, // No rounded corners
          //                                     ),
          //
          //                                   ),
          //                                 ),
          //
          //                               ),
          //
          //
          //                             ],
          //                           ),
          //                         )
          //
          //                       ],
          //                     ),
          //                   );
          //
          //                 }
          //                 )
          //
          //             )
          //
          //         ),
          //
          //       ),
          //         //////list the conducted exam here
          //
          //
          //     //   /////////// single student exams
          //       //   //////list the conducted exams here
          //
          //
          //       // Padding(padding: const EdgeInsets.all(8.0),
          //       //   child: SizedBox(
          //       //       height: 800,
          //       //       child:  ListView.builder(
          //       //           itemCount: examBox.length,
          //       //           itemBuilder: ((context, index){
          //       //             final exam = examBox.getAt(index) as Exam;
          //       //             print(exam.studentId);
          //       //
          //       //             return Card(
          //       //               shape: RoundedRectangleBorder( // Specify the shape of the Card
          //       //                 borderRadius: BorderRadius
          //       //                     .zero, // Set borderRadius to zero for sharp corners
          //       //               ),
          //       //               elevation: 2.0,
          //       //               // margin: EdgeInsets.all(8.0),
          //       //               child: Column(
          //       //                 crossAxisAlignment: CrossAxisAlignment.start,
          //       //                 children: [ // Card Header
          //       //                   Container(
          //       //                     padding: EdgeInsets.all(8.0),
          //       //                     color: const Color(0xffd9d9d9),
          //       //                     child: Row(
          //       //                       children: [
          //       //                         Text(
          //       //                           'Student ID:' + exam.studentId,
          //       //                           style: TextStyle(
          //       //                             fontWeight: FontWeight.bold,
          //       //                             // Makes the text bold
          //       //                             fontSize: 12.0,
          //       //                             // Increase the font size
          //       //                             color: Colors
          //       //                                 .black, // Darker blue color for the text
          //       //                           ),
          //       //                         ),
          //       //                       ],
          //       //                     ),
          //       //                   ),
          //       //
          //       //                   Container(
          //       //                     padding: EdgeInsets.all(8.0),
          //       //                     child: Column(
          //       //                       children: [
          //       //                         ListTile(
          //       //                           leading: Column(
          //       //                               children: [
          //       //                                 Text("Exam ID:"),
          //       //                                 // Text(exam.examId.toString()),
          //       //                                 Text(exam.key.toString()),
          //       //                               ]
          //       //                           ),
          //       //
          //       //                           title: Column(
          //       //                             children: [
          //       //                               Text("Date and time:"),
          //       //                               Text(
          //       //                                   formatDateTime(exam.createdAt)
          //       //                                       .toString()),
          //       //                             ],
          //       //                           ),
          //       //
          //       //
          //       //                           trailing: OutlinedButton(
          //       //                             onPressed: () {
          //       //                               Navigator.of(context).push(
          //       //                                 MaterialPageRoute(
          //       //                                   builder: (context) =>
          //       //                                       ExamDetails(exam: exam,
          //       //                                           student: widget
          //       //                                               .student),
          //       //                                 ),
          //       //                               );
          //       //                             },
          //       //                             child: Text('Details'),
          //       //                             style: OutlinedButton.styleFrom(
          //       //                               side: BorderSide(
          //       //                                   color: Color(0xffd3d3d3),
          //       //                                   width: 2.0),
          //       //                               foregroundColor: const Color(
          //       //                                   0xff000000),
          //       //
          //       //                               shape: RoundedRectangleBorder(
          //       //                                 borderRadius: BorderRadius
          //       //                                     .zero, // No rounded corners
          //       //                               ),
          //       //
          //       //                             ),
          //       //                           ),
          //       //
          //       //                         ),
          //       //
          //       //
          //       //                       ],
          //       //                     ),
          //       //                   )
          //       //
          //       //                 ],
          //       //               ),
          //       //             );
          //       //
          //       //           }
          //       //           )
          //       //
          //       //       )
          //       //
          //       //   ),
          //       //
          //       // ),
          //       //   //////list the conducted exam here
          //
          //     //   //////// single student exams
          //
          //     ],
          //   ),
          // ),



          // Center(child: Text('Lab tests')),
          // Center(child: Text('Prescriptions')),
        ],
      ),


      // FAB original start
      // floatingActionButton: _tabController.index == 1 ? FloatingActionButton.extended(
      //   foregroundColor: const Color(0xffffffff),
      //   backgroundColor: const Color(0xff18588d),
      //   label: Text("New Exam"),
      //   icon: Icon(Icons.add),
      //   onPressed: () {
      //     Navigator.push(
      //       context,
      //       MaterialPageRoute(
      //         builder: (context) => ConductExam(student: widget.student),
      //       ),
      //     );
      //   },
      //
      // ) : null,
      // FAB original end

      //   FAB 2 start
      floatingActionButtonLocation: ExpandableFab.location,
      floatingActionButton: ExpandableFab(
        key: _key,
        type: ExpandableFabType.up,
        distance: 100.0,

        openButtonBuilder: RotateFloatingActionButtonBuilder(
          child: const Icon(Icons.add),
          fabSize: ExpandableFabSize.regular,
          foregroundColor: const Color(0xffffffff),
          backgroundColor: const Color(0xff18588d),
          shape: const CircleBorder(),
        ),
        closeButtonBuilder: DefaultFloatingActionButtonBuilder(
          child: const Icon(Icons.close),
          fabSize: ExpandableFabSize.regular,
          foregroundColor: const Color(0xffffffff),
          backgroundColor: const Color(0xff18588d),
          shape: const CircleBorder(),
        ),

        overlayStyle: ExpandableFabOverlayStyle(
          // color: Colors.black.withOpacity(0.5),
          blur: 5,
        ),
        onOpen: () {
          debugPrint('onOpen');
        },
        afterOpen: () {
          debugPrint('afterOpen');
        },
        onClose: () {
          debugPrint('onClose');

        },
        afterClose: () {
          debugPrint('afterClose');
        },
        children: [
          // Container(
          //   width: 150, // Set your desired width here
          //   child: ElevatedButton(
          //     style: ButtonStyle(
          //       backgroundColor: MaterialStateProperty.all<Color>(Color(0xff18588d)), // Set button background color
          //       foregroundColor: MaterialStateProperty.all<Color>(Color(0xffffffff)),
          //       shape: MaterialStateProperty.all(
          //         RoundedRectangleBorder(
          //           borderRadius: BorderRadius.circular(8.0),
          //         ),
          //       ),
          //     ),
          //     onPressed: () {
          //       // Navigator.push(
          //       //   context,
          //       //   MaterialPageRoute(
          //       //     builder: (context) => EditRandomVitals(student: widget.student),
          //       //   ),
          //       // );
          //     },
          //     child: Text('Quick vitals'),
          //   ),
          // ),
          // Container(
          //   width: 150, // Set your desired width here
          //   child: ElevatedButton(
          //     style: ButtonStyle(
          //       backgroundColor: MaterialStateProperty.all<Color>(Color(0xff18588d)), // Set button background color
          //       foregroundColor: MaterialStateProperty.all<Color>(Color(0xffffffff)),
          //       shape: MaterialStateProperty.all(
          //         RoundedRectangleBorder(
          //           borderRadius: BorderRadius.circular(8.0),
          //         ),
          //       ),
          //     ),
          //     onPressed: () {
          //       Navigator.push(
          //         context,
          //         MaterialPageRoute(
          //           builder: (context) => ConductExam(student: widget.student),
          //         ),
          //       );
          //     },
          //     child: Text('Medical exam'),
          //   ),
          // ),
          Container(
            width: 150, // Set your desired width here
            child: ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Color(0xff18588d)), // Set button background color
                foregroundColor: MaterialStateProperty.all<Color>(Color(0xffffffff)),
                shape: MaterialStateProperty.all(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                ),
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => EditVaccination(student: widget.student),
                  ),
                );
              },
              child: Text('Vaccination'),
            ),
          ),

          Container(
            width: 150, // Set your desired width here
            child: ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Color(0xff18588d)), // Set button background color
                foregroundColor: MaterialStateProperty.all<Color>(Color(0xffffffff)),
                shape: MaterialStateProperty.all(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                ),
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => EditStudAllergies(student: widget.student),
                  ),
                );
              },
              child: Text('Allergies'),
            ),
          ),
          Container(
            width: 150, // Set your desired width here
            child: ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Color(0xff18588d)), // Set button background color
                foregroundColor: MaterialStateProperty.all<Color>(Color(0xffffffff)),
                shape: MaterialStateProperty.all(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                ),
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => EditStudMedicalHistory(student: widget.student),
                  ),
                );
              },
              child: Text('Medical History'),
            ),
          ),
          Container(
            width: 150, // Set your desired width here
            child: ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Color(0xff18588d)), // Set button background color
                foregroundColor: MaterialStateProperty.all<Color>(Color(0xffffffff)),
                shape: MaterialStateProperty.all(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                ),
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => EditStudFamilyHistory(student: widget.student),
                  ),
                );
              },
              child: Text('Family History'),
            ),
          ),
          Container(
            width: 150, // Set your desired width here
            child: ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Color(0xff18588d)), // Set button background color
                foregroundColor: MaterialStateProperty.all<Color>(Color(0xffffffff)),
                shape: MaterialStateProperty.all(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                ),
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => EditStudChronic(student: widget.student),
                  ),
                );
              },
              child: Text('Chronic Illness'),
            ),
          ),

        ],
      ),

      //   FAB 2 end

    );
  }

  String formatDateTime(DateTime dateTime) {
    // Create a DateFormat with the desired format
    final DateFormat formatter = DateFormat('yyyy-MM-dd HH:mm');
    // Return the formatted date and time string
    return formatter.format(dateTime);
  }

  int calculateAge(DateTime birthdate) {
    DateTime currentDate = DateTime.now();
    int age = currentDate.year - birthdate.year;
    if (birthdate.month > currentDate.month ||
        (birthdate.month == currentDate.month && birthdate.day > currentDate.day)) {
      age--;
    }
    return age;
  }
}


class TemperatureLineChart extends StatelessWidget {
  final List<FlSpot> temperatureSpots = [
    FlSpot(1, 36.6),
    FlSpot(2, 37.0),
    FlSpot(3, 36.8),
    FlSpot(4, 37.2),
    FlSpot(5, 36.5),
  ];

  @override
  Widget build(BuildContext context) {
    // Ensure the chart is within a bounded context
    return Container(
      height: 300, // Specify a height for the container
      padding: const EdgeInsets.all(25.0),
      child: LineChart(
        LineChartData(
          gridData: FlGridData(show: false),
          titlesData: FlTitlesData(
            bottomTitles: AxisTitles(
              sideTitles: SideTitles(
                showTitles: true,
                reservedSize: 22, // Adjust if needed
                getTitlesWidget: _bottomTitleWidgets,
              ),
            ),
            leftTitles: AxisTitles(
              sideTitles: SideTitles(
                showTitles: true,
                reservedSize: 28, // Adjust if needed
                getTitlesWidget: _leftTitleWidgets,
              ),
            ),
            topTitles: AxisTitles(
              sideTitles: SideTitles(showTitles: false), // This line hides the top titles
            ),
            rightTitles: AxisTitles(
              sideTitles: SideTitles(showTitles: false),
            ),
          ),
          borderData: FlBorderData(show: true, border: Border.all(color: const Color(0xff37434d), width: 1)),
          minX: 1,
          maxX: 5,
          minY: 36,
          maxY: 38,
          lineBarsData: [
            LineChartBarData(
              spots: temperatureSpots,
              isCurved: true,
              color: const Color(0xff18588d),
              barWidth: 3,
              isStrokeCapRound: true,
              dotData: FlDotData(show: true),
              belowBarData: BarAreaData(show: false),
            ),
          ],
        ),
      ),
    );
  }

  Widget _bottomTitleWidgets(double value, TitleMeta meta) {
    const style = TextStyle(
      color: Color(0xff68737d),
      fontWeight: FontWeight.bold,
      fontSize: 16,
    );

    // Define a map of the exact x-values and their corresponding labels
    Map<int, String> dateLabels = {
      1: 'Mar 1',
      2: 'Mar 2',
      3: 'Mar 3',
      4: 'Mar 4',
      5: 'Mar 5',
    };

    // Check if the current value has a corresponding label, otherwise return an empty widget
    return dateLabels.containsKey(value.toInt())
        ? SideTitleWidget(
      axisSide: meta.axisSide,
      child: Text(dateLabels[value.toInt()]!, style: style),
      space: 8.0, // Adjust spacing as needed
    )
        : SideTitleWidget(
      axisSide: meta.axisSide,
      child: Text('', style: style),
      space: 8.0, // Maintain spacing for alignment
    );
  }

  Widget _leftTitleWidgets(double value, TitleMeta meta) {
    final style = TextStyle(
      color: Color(0xff67727d),
      fontWeight: FontWeight.bold,
      fontSize: 15,
    );

    // Preventing "Infinity or NaN toInt" by validating value
    String text = value.isFinite ? '${value.toStringAsFixed(1)}': '';
    return SideTitleWidget(
      axisSide: meta.axisSide,
      child: Text(text, style: style),
      space: 4, // Adjust spacing if needed
    );
  }
}

class TemperatureDonutChart extends StatelessWidget {
  final double temperature;
  final double normalTemperature = 36.8; // Normal temperature

  TemperatureDonutChart({Key? key, required this.temperature}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      width: 200,
      child: PieChart(
        PieChartData(
          sectionsSpace: 0, // No space between sections
          centerSpaceRadius: 30, // Radius of the donut hole
          startDegreeOffset: -90, // Start from the top
          sections: [
            PieChartSectionData(
              color: _getSectionColor(),
              value: 100, // Single section fills the whole chart
              radius: 50, // Outer radius of the section
              title: '${temperature.toStringAsFixed(1)}°C',
              titleStyle: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 16,
              ),
              showTitle: true,
            ),
          ],
        ),
      ),
    );

  }

  // Determine the color based on the temperature
  Color _getSectionColor() {
    if (temperature == normalTemperature) {
      return Colors.green;
    } else if (temperature < normalTemperature) {
      return Colors.orange;
    } else {
      return Colors.red;
    }
  }
}

class TemperatureList extends StatelessWidget {
  final List<double> temperatures = [36.6, 37.0, 36.8, 37.2, 36.5]; // Example temperatures

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: temperatures.length,
      itemBuilder: (context, index) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: TemperatureDonutChart(temperature: temperatures[index]),
        );
      },
    );
  }
}

// /////// card tabs starts here
class MyHorizontalListWithNavigation extends StatefulWidget {
  @override
  _MyHorizontalListWithNavigationState createState() => _MyHorizontalListWithNavigationState();
}

class _MyHorizontalListWithNavigationState extends State<MyHorizontalListWithNavigation> {
  final ScrollController _scrollController = ScrollController();
  int _selectedIndex = -1; // Variable to hold the index of the selected button

  final List<String> buttonNames = [
    'Temperature',
    'Weight',
    'Height',
    'Glucose',
    'SpO2',
    'PRbpm',
    'PUL',
    'SYS',
    'DIA'
  ];

  @override
  Widget build(BuildContext context) {
    return Column( // Wrap in a Column to display content below the horizontal list
      children: [
        Row(
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                _scrollController.animateTo(
                  _scrollController.offset - 150, // Change 100 to desired scroll amount
                  duration: Duration(milliseconds: 500),
                  curve: Curves.easeInOut,
                );
              },
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.symmetric(vertical: 20.0),
                height: 50.0,
                child: ListView.builder(
                  controller: _scrollController,
                  scrollDirection: Axis.horizontal,
                  itemCount: buttonNames.length, // Number of buttons
                  itemBuilder: (context, index) {
                    return Container(
                      width: 150.0,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: OutlinedButton(
                          style: OutlinedButton.styleFrom(
                            side: BorderSide(color: const Color(0xff18588d),),
                            foregroundColor: Colors.black, // Text Color
                            // backgroundColor: Colors.blue,
                            shape: RoundedRectangleBorder( // Making the button's corners slightly rounded
                              borderRadius: BorderRadius.circular(0),
                            ),// Button background color
                          ),
                          onPressed: () {
                            setState(() {
                              _selectedIndex = index; // Update selected index
                            });
                          },
                          child: Text(buttonNames[index]),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
            IconButton(
              icon: Icon(Icons.arrow_forward),
              onPressed: () {
                _scrollController.animateTo(
                  _scrollController.offset + 100, // Change 100 to desired scroll amount
                  duration: Duration(milliseconds: 500),
                  curve: Curves.easeInOut,
                );
              },
            ),
          ],
        ),
        Expanded(
          child: _buildSelectedContent(_selectedIndex), // Method to build the selected content
        ),
      ],
    );
  }

  Widget _buildSelectedContent(int index) {
    // Depending on the selected index, return different widgets
    switch (index) {
      case 0: // Temperature
        return TemperatureDataView();
    //   ListView.builder(
    //   itemCount: 10, // Example item count for a list
    //   itemBuilder: (BuildContext context, int index) {
    //     return ListTile(title: Text('Temperature Reading $index'));
    //   },
    // );
      case 1: // Weight
        return Text('Weight Data Display');
      case 2: // Height
        return SingleChildScrollView(
          child: Column(
            children: List.generate(5, (index) => Text('Height Measurement $index')),
          ),
        );
    // Add cases for other indices as needed
      default:
        return Center(child: Text('Please select an option above')); // Default view
    }
  }

  @override
  void dispose() {
    _scrollController.dispose(); // Always dispose controller
    super.dispose();
  }
}
// ///// card tabs ends here

// temperature preview code starts here

class TemperatureDataView extends StatefulWidget {
  @override
  _TemperatureDataViewState createState() => _TemperatureDataViewState();
}

class _TemperatureDataViewState extends State<TemperatureDataView> {
  bool _isListView = true; // Initial view is set to list

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        // Toggle switch
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text('List'),
              Switch(
                value: _isListView,
                onChanged: (value) {
                  setState(() {
                    _isListView = value;
                  });
                },
              ),
              const Text('Graph'),
            ],
          ),
        ),
        // Conditional display based on _isListView
        Expanded(
          child: _isListView ? TemperatureTable() : TemperatureLineChart(),
        ),
      ],
    );
  }


}

// temperature preview code ends here


TableRow _buildTableRow(String label, String value) {
  return TableRow(
    children: [
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 5.0),
        child: Text(
          label,
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 5.0),
        child: Text(
          value,
          style: TextStyle(
            fontSize: 16.0,
          ),
        ),
      ),
    ],
  );
}
