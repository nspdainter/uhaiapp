import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_expandable_fab/flutter_expandable_fab.dart';
import 'package:hive/hive.dart';
import 'package:uhaiapp/nosql_models/exam.dart';
import 'package:uhaiapp/nurse/edit_allergies.dart';
import 'package:uhaiapp/nurse/edit_chronic_illlness.dart';
import 'package:uhaiapp/nurse/edit_family_history.dart';
import 'package:uhaiapp/nurse/edit_medical_history.dart';
import 'package:uhaiapp/nurse/edit_vaccination.dart';
import 'package:uhaiapp/nurse/medical_profile.dart';
import 'package:uhaiapp/nurse/studts.dart';
import '../nosql_models/student.dart';
import '../nosql_models/astudent.dart';
import 'conductExam.dart'; //
import 'package:fl_chart/fl_chart.dart';
import 'package:intl/intl.dart';
import 'examDetails.dart';
import 'localStudents.dart';
import 'vitalsCards/vitalsCards.dart';


// FAB variable
final scaffoldKey = GlobalKey<ScaffoldMessengerState>();
// FAB variable

class StudtDetails extends StatefulWidget {
  final Studt student;


  const StudtDetails({
    Key? key,
    required this.student,
  }) : super(key: key);

  @override
  _StudtDetailsState createState() => _StudtDetailsState();
}

class _StudtDetailsState extends State<StudtDetails> with SingleTickerProviderStateMixin {

  // vitals initializer start

  final double bmiValue = 31.0;
  final double tempValue = 39.9;

  // vital initializer end


  late TabController _tabController;

  late Box<Exam> examBox;

  // FAB
  final _key = GlobalKey<ExpandableFabState>();
  // FAB

  bool isExpanded = false; // ExpansionTile state




  @override
  void initState() {
    super.initState();
    examBox = Hive.box<Exam>('exams');
    _tabController = TabController(length: 3, vsync: this);
    // Update the UI when the tab changes
    _tabController.addListener(() {
      // Force widget rebuild on tab change
      if (!mounted) return; // Check if the widget is still in the widget tree
      setState(() {
        // The UI will rebuild, and the FloatingActionButton's visibility will be updated
      });
    });

    //   FAB

    //   FAB

  }

  @override
  void dispose() {
    _tabController.dispose(); // Dispose of the TabController to clean up resources

    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    // final Student student = studentBox.getAt(index) as Student;
    String? firstname = widget.student.firstName;
    String? lastname = widget.student.lastName!;
    String? initials = (firstname != null && lastname != null) ? "${firstname[0]}${lastname[0]}" : "!";
    return Scaffold(
      appBar: AppBar(
        bottom: TabBar(
          isScrollable: true,
          indicatorColor: const Color(0xff70c05d),
          unselectedLabelColor: const Color(0xffffffff),
          labelColor: const Color(0xff70c05d),
          controller: _tabController,
          tabs: [
            Tab(text: "OVERVIEW"),
            Tab(text: "MEDICAL PROFILE"),
            Tab(text: "MEDICAL EXAMS"),
            // Tab(text: "LAB TESTS"),
            // Tab(text: "PRESCRIPTIONS"),
          ],
        ),
        backgroundColor: const Color(0xff18588d),
        title: Text(
            widget.student.firstName!,
            style: TextStyle(color: const Color(0xffffffff))
        ),
        iconTheme: IconThemeData(
          color: const Color(0xffffffff),
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        children: [

          SingleChildScrollView(
            child: Column (
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Card(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          padding: EdgeInsets.all(8.0),
                          color: const Color(0xff949494), // Grey colored heading section
                          width: double.infinity, // Makes the container take full width
                          child: Text(
                            'Bio data',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        SizedBox(height: 10,),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              height: 128,
                              width: 128,
                              color: Colors.grey,
                              child: Center(
                                child: Text(
                                  initials.toUpperCase(),
                                  style: TextStyle(
                                    fontSize: 80,
                                    color: const Color(0xff000000),
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(width: 20,),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        Text(
                                          widget.student.firstName.toString(),
                                          style: TextStyle(
                                            fontSize: 24,
                                            fontWeight: FontWeight.bold,
                                            color: Color(0xff18588d),
                                          ),
                                        ),
                                        SizedBox(width: 40,),
                                        // Text(
                                        //   widget.student.toString(),
                                        //   style: TextStyle(
                                        //     fontSize: 24,
                                        //     fontWeight: FontWeight.bold,
                                        //     color: Color(0xff18588d),
                                        //   ),
                                        // ),
                                      ],
                                    ),
                                    Container(
                                      padding: const EdgeInsets.only(bottom: 2),
                                      child: Text(
                                        widget.student.gender! == "M" ? "MALE" : "FEMALE",
                                        style: TextStyle(fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "Class:    Senior 2",
                                        style: TextStyle(fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "UID:${widget.student.studentUniqueId}",
                                        style: TextStyle(fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "Tribe:    Mugweri",
                                        style: TextStyle(fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "Home address:    Makindye",
                                        style: TextStyle(fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            // SizedBox(width: 20),
                            // Flexible(
                            //   child: Column(
                            //     crossAxisAlignment: CrossAxisAlignment.end,
                            //     children: [
                            //       BMICard(bmi: bmiValue),
                            //       Card(
                            //         child: Container(
                            //           width: 100,
                            //           height: 60,
                            //           color: const Color(0xffc8c8c8),
                            //           child: Column(
                            //             children: [
                            //               SizedBox(height: 8,),
                            //               Text('Height: (cm)'),
                            //               Text('163.1', style: TextStyle(fontWeight: FontWeight.bold),)
                            //             ],
                            //           ),
                            //         ),
                            //       ),
                            //       Card(
                            //         child: Container(
                            //           width: 100,
                            //           height: 60,
                            //           color: const Color(0xffc8c8c8),
                            //           child: Column(
                            //             children: [
                            //               SizedBox(height: 8,),
                            //               Text('Blood Group:'),
                            //               Text('AB', style: TextStyle(fontWeight: FontWeight.bold),)
                            //             ],
                            //           ),
                            //         ),
                            //       ),
                            //     ],
                            //   ),
                            // ),
                          ],
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Flexible(
                              child: Row(
                                // crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  BMICard(bmi: bmiValue),
                                  Card(
                                    child: Container(
                                      width: 100,
                                      height: 60,
                                      color: const Color(0xffc8c8c8),
                                      child: Column(
                                        children: [
                                          SizedBox(height: 8,),
                                          Text('Height: (cm)'),
                                          Text('163.1', style: TextStyle(fontWeight: FontWeight.bold),)
                                        ],
                                      ),
                                    ),
                                  ),
                                  Card(
                                    child: Container(
                                      width: 100,
                                      height: 60,
                                      color: const Color(0xffc8c8c8),
                                      child: Column(
                                        children: [
                                          SizedBox(height: 8,),
                                          Text('Blood Group:'),
                                          Text('AB', style: TextStyle(fontWeight: FontWeight.bold),)
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Card(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [

                        Container(
                          padding: EdgeInsets.all(8.0),
                          color: const Color(0xff949494), // Grey colored heading section
                          width: double.infinity, // Makes the container take full width
                          child: Text(
                            'Last Vitals',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Wrap(
                            spacing: 8.0, // space between cards horizontally
                            runSpacing: 8.0, // space between cards vertically
                            children: <Widget>[
                              // Add your Card widgets here
                              TempCard(temp: tempValue),
                              Card(
                                child: Container(
                                  width: 100,
                                  height: 60,
                                  color: const Color(0xffc8c8c8),
                                  child: Column(children: [SizedBox(height: 8,),Text('Weight:'),Text('57.3 kgs', style: TextStyle(fontWeight: FontWeight.bold),)]),
                                ),
                              ),
                              Card(
                                child: Container(
                                  width: 100,
                                  height: 60,
                                  color: const Color(0xffc8c8c8),
                                  child: Column(children: [SizedBox(height: 8,),Text('Height:'),Text('134.1 cm', style: TextStyle(fontWeight: FontWeight.bold),)]),
                                ),
                              ),
                              Card(
                                child: Container(
                                  width: 100,
                                  height: 60,
                                  color: const Color(0xffc8c8c8),
                                  child: Column(children: [SizedBox(height: 8,),Text('Glucose:'),Text('80 mg/dL', style: TextStyle(fontWeight: FontWeight.bold),)]),
                                ),
                              ),
                              Card(
                                child: Container(
                                  width: 100,
                                  height: 60,
                                  color: const Color(0xffc8c8c8),
                                  child: Column(children: [SizedBox(height: 8,),Text('Sp02:'),Text('180', style: TextStyle(fontWeight: FontWeight.bold),)]),
                                ),
                              ),
                              Card(
                                child: Container(
                                  width: 100,
                                  height: 60,
                                  color: const Color(0xffc8c8c8),
                                  child: Column(children: [SizedBox(height: 8,),Text('PRbpm:'),Text('83', style: TextStyle(fontWeight: FontWeight.bold),)]),
                                ),
                              ),

                              Card(
                                child: Container(
                                  width: 100,
                                  height: 60,
                                  color: const Color(0xffc8c8c8),
                                  child: Column(children: [SizedBox(height: 8,),Text('SYS:'),Text('62 mmHg', style: TextStyle(fontWeight: FontWeight.bold),)]),
                                ),
                              ),
                              Card(
                                child: Container(
                                  width: 100,
                                  height: 60,
                                  color: const Color(0xffc8c8c8),
                                  child: Column(children: [SizedBox(height: 8,),Text('DIA:'),Text('176 mmHg', style: TextStyle(fontWeight: FontWeight.bold),)]),
                                ),
                              ),
                              Card(
                                child: Container(
                                  width: 100,
                                  height: 60,
                                  color: const Color(0xffc8c8c8),
                                  child: Column(children: [SizedBox(height: 8,),Text('PUL:'),Text('17 /min', style: TextStyle(fontWeight: FontWeight.bold),)]),
                                ),
                              ),
                              // Add more cards as needed
                            ],
                          ),
                        ),
                        SizedBox(height: 40,),

                      ],
                    ),
                  ),
                ),
                SizedBox(height: 20,),

                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: ExpansionTile(
                    initiallyExpanded:true,
                    collapsedBackgroundColor: const Color(0xff949494),
                    collapsedTextColor: const Color(0xff000000),
                    title: Text('Chronic Illnesses', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                    trailing: Icon(Icons.expand_more),
                    children: <Widget>[
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: EdgeInsets.all(8.0),
                          // child: Wrap(
                          //   alignment: WrapAlignment.start,
                          //   spacing: 8.0,
                          //   children: widget.student.chronicIllness.map((illness) {
                          //     return InputChip(
                          //       onSelected: (bool value) {},
                          //       backgroundColor: Color(0xffc8c8c8),
                          //       label: Text(
                          //         illness['condition_name'],
                          //         style: TextStyle(color: Color(0xff000000)),
                          //       ),
                          //       avatar: Icon(
                          //         Icons.check,
                          //         size: 20.0,
                          //         color: Color(0xff000000),
                          //       ),
                          //     );
                          //   }).toList(),
                          // ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text("Comment", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                                SizedBox(height: 15,),
                                // Text(widget.student.chronicIllnessComment!),
                              ],

                            )
                        ),

                      )

                    ],
                  ),
                ),
                const SizedBox(height: 20,),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: ExpansionTile(
                    initiallyExpanded:true,
                    collapsedBackgroundColor: Color(0xff949494),
                    collapsedTextColor: Color(0xff000000),
                    title: Text('Allergies', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                    trailing: Icon(Icons.expand_more),
                    children: <Widget>[
                      // Align(
                      //   alignment: Alignment.centerLeft,
                      //   child: Padding(
                      //     padding: EdgeInsets.all(8.0),
                      //     child: Wrap(
                      //       alignment: WrapAlignment.start,
                      //       spacing: 8.0,
                      //       children: widget.student.allergies.map((allergy) {
                      //         return InputChip(
                      //           onSelected: (bool value) {},
                      //           backgroundColor: Color(0xffc8c8c8),
                      //           label: Text(
                      //             allergy['name'],
                      //             style: TextStyle(color: Color(0xff000000)),
                      //           ),
                      //           avatar: Icon(
                      //             Icons.check,
                      //             size: 20.0,
                      //             color: Color(0xff000000),
                      //           ),
                      //         );
                      //       }).toList(),
                      //     ),
                      //   ),
                      // ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text("Comment", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                                SizedBox(height: 15,),
                                // Text(widget.student.allergiesComment!),
                              ],

                            )
                        ),

                      )
                    ],
                  ),
                ),

                const SizedBox(height: 20,),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: ExpansionTile(
                    initiallyExpanded:true,
                    collapsedBackgroundColor: Color(0xff949494),
                    collapsedTextColor: Color(0xff000000),
                    title: Text('Medical History', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                    trailing: Icon(Icons.expand_more),
                    children: <Widget>[
                      // Align(
                      //   alignment: Alignment.centerLeft,
                      //   child: Padding(
                      //     padding: EdgeInsets.all(8.0),
                      //     child: Wrap(
                      //       alignment: WrapAlignment.start,
                      //       spacing: 8.0,
                      //       children: widget.student.medicalHistory.map((medicalhistory) {
                      //         return InputChip(
                      //           onSelected: (bool value) {},
                      //           backgroundColor: Color(0xffc8c8c8),
                      //           label: Text(
                      //             medicalhistory['condition_name'],
                      //             style: TextStyle(color: Color(0xff000000)),
                      //           ),
                      //           avatar: Icon(
                      //             Icons.check,
                      //             size: 20.0,
                      //             color: Color(0xff000000),
                      //           ),
                      //         );
                      //       }).toList(),
                      //     ),
                      //   ),
                      // ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text("Comments", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                                SizedBox(height: 15,),
                                // Text(widget.student.medicalHistoryComment!),
                              ],

                            )
                        ),

                      )

                    ],
                  ),
                ),

              ],),
          ),
          //last vitals start here

          // SingleChildScrollView(
          //   child: Column (
          //     children: [
          //       Padding(
          //         padding: const EdgeInsets.all(8.0),
          //         child: Card(
          //           child: Column(
          //             crossAxisAlignment: CrossAxisAlignment.start,
          //             children: [
          //               Container(
          //                 padding: EdgeInsets.all(8.0),
          //                 color: const Color(0xff949494), // Grey colored heading section
          //                 width: double.infinity, // Makes the container take full width
          //                 child: Text(
          //                   'Detailed Vitals Exams',
          //                   style: TextStyle(
          //                     fontWeight: FontWeight.bold,
          //                   ),
          //                 ),
          //               ),
          //
          //             ],
          //           ),
          //         ),
          //       ),
          //
          //       Padding(padding: const EdgeInsets.all(8.0),
          //           child: SizedBox(
          //             height: 500,
          //             child: MyHorizontalListWithNavigation(),)
          //
          //       ),
          //
          //       Padding(padding: const EdgeInsets.all(8.0),
          //         child: SizedBox(
          //           // height: 500,
          //           child: Container(
          //             padding: EdgeInsets.all(8.0),
          //             color: const Color(0xff949494), // Grey colored heading section
          //             width: double.infinity, // Makes the container take full width
          //             child: Text(
          //               'Detailed Medical Exams',
          //               style: TextStyle(
          //                 fontWeight: FontWeight.bold,
          //               ),
          //             ),
          //           ),
          //         ),),
          //
          //       //   //////list the conducted exams here
          //       Padding(padding: const EdgeInsets.all(8.0),
          //         child: SizedBox(
          //             height: 500,
          //             child: ListView.builder(
          //                 itemCount: examBox.length,
          //                 itemBuilder: ((context, index){
          //                   final exam = examBox.getAt(index) as Exam;
          //                   return Card(
          //                     shape: RoundedRectangleBorder( // Specify the shape of the Card
          //                       borderRadius: BorderRadius.zero, // Set borderRadius to zero for sharp corners
          //                     ),
          //                     elevation: 2.0,
          //                     // margin: EdgeInsets.all(8.0),
          //                     child: Column(
          //                       crossAxisAlignment: CrossAxisAlignment.start,
          //                       children: [ // Card Header
          //                         Container(
          //                           padding: EdgeInsets.all(8.0),
          //                           color: const Color(0xffd9d9d9),
          //                           child: Row(
          //                             children: [
          //                               Text(
          //                                 'Student ID:' + exam.studentId,
          //                                 style: TextStyle(
          //                                   fontWeight: FontWeight.bold, // Makes the text bold
          //                                   fontSize: 12.0, // Increase the font size
          //                                   color: Colors.black, // Darker blue color for the text
          //                                 ),
          //                               ),
          //                             ],
          //                           ),
          //                         ),
          //
          //                         Container(
          //                           padding: EdgeInsets.all(8.0),
          //                           child: Column(
          //                             children: [
          //                               ListTile(
          //                                 leading: Column(
          //                                     children: [
          //                                       Text("Exam ID:"),
          //                                       // Text(exam.examId.toString()),
          //                                       Text(exam.key.toString()),
          //                                     ]
          //                                 ),
          //
          //                                 title: Column(
          //                                   children: [
          //                                     Text("Date and time:"),
          //                                     Text(formatDateTime(exam.createdAt).toString()),
          //                                   ],
          //                                 ),
          //
          //
          //
          //                                 trailing: OutlinedButton(
          //                                   onPressed: () {
          //
          //                                     Navigator.of(context).push(
          //                                       MaterialPageRoute(
          //                                         builder: (context) => ExamDetails(exam: exam),
          //                                       ),
          //                                     );
          //
          //
          //                                   },
          //                                   child: Text('Details'),
          //                                   style: OutlinedButton.styleFrom(
          //                                     side: BorderSide(color: Color(0xffd3d3d3), width: 2.0),
          //                                     foregroundColor: const Color(0xff000000),
          //
          //                                     shape: RoundedRectangleBorder(
          //                                       borderRadius: BorderRadius.zero, // No rounded corners
          //                                     ),
          //
          //                                   ),
          //                                 ),
          //
          //                               ),
          //
          //
          //
          //                             ],
          //                           ),
          //                         )
          //
          //                       ],
          //                     ),
          //                   );
          //                 }
          //                 )
          //
          //             )
          //
          //         ),
          //
          //       ),
          //       //   //////list the conducted exam here
          //
          //     ],),
          // ),


          Center(child: Text('Full Medical Profile')),
          SingleChildScrollView(
            child: Column (
              children: [
                // search bar start
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextField(
                    // controller: searchController,
                    decoration: const InputDecoration(
                      labelText: 'Search',
                      suffixIcon: Icon(Icons.search),
                    ),
                    onChanged: (value) {
                      // setState(() {
                      //   searchQuery = value.toLowerCase();
                      // });
                    },
                  ),
                ),
                // search bar end
                Column(
                    children: [
                      ListView.builder(
                          shrinkWrap: true,
                          itemCount: examBox.length,
                          itemBuilder: ((context, index){
                            final exam = examBox.getAt(index) as Exam;
                            return Card(
                              shape: const RoundedRectangleBorder( // Specify the shape of the Card
                                borderRadius: BorderRadius.zero, // Set borderRadius to zero for sharp corners
                              ),
                              elevation: 2.0,
                              // margin: EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [ // Card Header
                                  Container(
                                    padding: const EdgeInsets.all(8.0),
                                    color: const Color(0xffd9d9d9),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          'Student ID: ${exam.studentId}',
                                          style: const TextStyle(
                                            fontWeight: FontWeight.bold, // Makes the text bold
                                            fontSize: 12.0, // Increase the font size
                                            color: Colors.black, // Darker blue color for the text
                                          ),
                                        ),
                                        // const SizedBox(width: 100,),

                                        Text(
                                          'Name:${exam.studentId}',
                                          style: const TextStyle(
                                            fontWeight: FontWeight.bold, // Makes the text bold
                                            fontSize: 12.0, // Increase the font size
                                            color: Colors.black, // Darker blue color for the text
                                          ),
                                        ),

                                        Container(
                                          // child: IconButton(
                                          //   icon: Icon(Icons.delete),
                                          //   onPressed: () {
                                          //     // Add your delete functionality here
                                          //     // print('Delete button pressed');
                                          //   },
                                          //   // color: Colors.red, // You can customize the color
                                          //   // iconSize: 10, // You can customize the size
                                          // ),
                                        ),
                                        Container(
                                          child: IconButton(
                                            icon: Icon(Icons.delete),
                                            onPressed: () async {
                                              var examKey = exam.key.toString();
                                              print('Exam with id: $examKey has been clicked.');
                                            },
                                            color: Colors.grey, // You can customize the color
                                            // iconSize: 10, // You can customize the size
                                          ),
                                        )


                                      ],
                                    ),
                                  ),

                                  Container(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(
                                      children: [
                                        ListTile(
                                          leading: Column(
                                              children: [
                                                const Text("Exam ID:"),
                                                // Text(exam.examId.toString()),
                                                Text(exam.key.toString()),
                                              ]
                                          ),

                                          title:
                                          Row(
                                              mainAxisSize: MainAxisSize.min,
                                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                              children: [
                                                Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    const Text("Created:"),
                                                    Text(formatDateTime(exam.createdAt).toString()),
                                                  ],
                                                ),
                                                Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    const Text("Reviewed:"),
                                                    Text(formatDateTime(exam.createdAt).toString()),
                                                  ],
                                                ),
                                              ]

                                          ),



                                          trailing: OutlinedButton(
                                            onPressed: () {


                                              // Navigator.of(context).push(
                                              //   MaterialPageRoute(
                                              //     builder: (context) => ExamDetails(exam: exam),
                                              //   ),
                                              // );


                                            },
                                            style: OutlinedButton.styleFrom(
                                              side: const BorderSide(color: Color(0xffd3d3d3), width: 2.0),
                                              foregroundColor: const Color(0xff000000),

                                              shape: const RoundedRectangleBorder(
                                                borderRadius: BorderRadius.zero, // No rounded corners
                                              ),

                                            ),
                                            child: const Text('Details'),
                                          ),

                                        ),



                                      ],
                                    ),
                                  )

                                ],
                              ),
                            );
                          }
                          )

                      )
                    ]


                ),



                //   //////list the conducted exams here
                // Padding(padding: const EdgeInsets.all(8.0),
                //   child: SizedBox(
                //       height: 500,
                //       child: ListView.builder(
                //           itemCount: examBox.length,
                //           itemBuilder: ((context, index){
                //             final exam = examBox.getAt(index) as Exam;
                //             return Card(
                //               shape: const RoundedRectangleBorder( // Specify the shape of the Card
                //                 borderRadius: BorderRadius.zero, // Set borderRadius to zero for sharp corners
                //               ),
                //               elevation: 2.0,
                //               // margin: EdgeInsets.all(8.0),
                //               child: Column(
                //                 crossAxisAlignment: CrossAxisAlignment.start,
                //                 children: [ // Card Header
                //                   Container(
                //                     padding: const EdgeInsets.all(8.0),
                //                     color: const Color(0xffd9d9d9),
                //                     child: Row(
                //                       mainAxisAlignment: MainAxisAlignment.start,
                //                       children: [
                //                         Text(
                //                           'Student ID: ${exam.studentId}',
                //                           style: const TextStyle(
                //                             fontWeight: FontWeight.bold, // Makes the text bold
                //                             fontSize: 12.0, // Increase the font size
                //                             color: Colors.black, // Darker blue color for the text
                //                           ),
                //                         ),
                //                         const SizedBox(width: 100,),
                //
                //                         Text(
                //                           'Name:${exam.studentId}',
                //                           style: const TextStyle(
                //                             fontWeight: FontWeight.bold, // Makes the text bold
                //                             fontSize: 12.0, // Increase the font size
                //                             color: Colors.black, // Darker blue color for the text
                //                           ),
                //                         ),
                //
                //
                //                       ],
                //                     ),
                //                   ),
                //
                //                   Container(
                //                     padding: const EdgeInsets.all(8.0),
                //                     child: Column(
                //                       children: [
                //                         ListTile(
                //                           leading: Column(
                //                               children: [
                //                                 const Text("Exam ID:"),
                //                                 // Text(exam.examId.toString()),
                //                                 Text(exam.key.toString()),
                //                               ]
                //                           ),
                //
                //                           title:
                //                           Row(
                //                               mainAxisSize: MainAxisSize.min,
                //                               mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                //                               children: [
                //                                 Column(
                //                                   crossAxisAlignment: CrossAxisAlignment.start,
                //                                   children: [
                //                                     const Text("Created:"),
                //                                     Text(formatDateTime(exam.createdAt).toString()),
                //                                   ],
                //                                 ),
                //                                 Column(
                //                                   crossAxisAlignment: CrossAxisAlignment.start,
                //                                   children: [
                //                                     const Text("Reviewed:"),
                //                                     Text(formatDateTime(exam.createdAt).toString()),
                //                                   ],
                //                                 ),
                //                               ]
                //
                //                           ),
                //
                //
                //
                //                           trailing: OutlinedButton(
                //                             onPressed: () {
                //
                //
                //                               Navigator.of(context).push(
                //                                 MaterialPageRoute(
                //                                   builder: (context) => ExamDetails(exam: exam),
                //                                 ),
                //                               );
                //
                //
                //                             },
                //                             style: OutlinedButton.styleFrom(
                //                               side: const BorderSide(color: Color(0xffd3d3d3), width: 2.0),
                //                               foregroundColor: const Color(0xff000000),
                //
                //                               shape: const RoundedRectangleBorder(
                //                                 borderRadius: BorderRadius.zero, // No rounded corners
                //                               ),
                //
                //                             ),
                //                             child: const Text('Details'),
                //                           ),
                //
                //                         ),
                //
                //
                //
                //                       ],
                //                     ),
                //                   )
                //
                //                 ],
                //               ),
                //             );
                //           }
                //           )
                //
                //       )
                //
                //   ),
                //
                // ),
                //   //////list the conducted exam here

              ],),
          ),
          // Center(child: Text('Medical Exams')),
          // Center(child: Text('Prescriptions')),
        ],
      ),


      // FAB original start
      // floatingActionButton: _tabController.index == 1 ? FloatingActionButton.extended(
      //   foregroundColor: const Color(0xffffffff),
      //   backgroundColor: const Color(0xff18588d),
      //   label: Text("New Exam"),
      //   icon: Icon(Icons.add),
      //   onPressed: () {
      //     Navigator.push(
      //       context,
      //       MaterialPageRoute(
      //         builder: (context) => ConductExam(student: widget.student),
      //       ),
      //     );
      //   },
      //
      // ) : null,
      // FAB original end

      //   FAB 2 start
      floatingActionButtonLocation: ExpandableFab.location,
      floatingActionButton: ExpandableFab(
        key: _key,
        type: ExpandableFabType.up,
        distance: 120.0,

        openButtonBuilder: RotateFloatingActionButtonBuilder(
          child: const Icon(Icons.add),
          fabSize: ExpandableFabSize.regular,
          foregroundColor: const Color(0xffffffff),
          backgroundColor: const Color(0xff18588d),
          shape: const CircleBorder(),
        ),
        closeButtonBuilder: DefaultFloatingActionButtonBuilder(
          child: const Icon(Icons.close),
          fabSize: ExpandableFabSize.regular,
          foregroundColor: const Color(0xffffffff),
          backgroundColor: const Color(0xff18588d),
          shape: const CircleBorder(),
        ),

        overlayStyle: ExpandableFabOverlayStyle(
          // color: Colors.black.withOpacity(0.5),
          blur: 5,
        ),
        onOpen: () {
          debugPrint('onOpen');
        },
        afterOpen: () {
          debugPrint('afterOpen');
        },
        onClose: () {
          debugPrint('onClose');

        },
        afterClose: () {
          debugPrint('afterClose');
        },
        children: [
          Container(
            width: 150, // Set your desired width here
            child: ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Color(0xff18588d)), // Set button background color
                foregroundColor: MaterialStateProperty.all<Color>(Color(0xffffffff)),
                shape: MaterialStateProperty.all(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                ),
              ),
              onPressed: () {
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(
                //     builder: (context) => EditVaccination(student: widget.student),
                //   ),
                // );
              },
              child: Text('Vaccination'),
            ),
          ),
          Container(
            width: 150, // Set your desired width here
            child: ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Color(0xff18588d)), // Set button background color
                foregroundColor: MaterialStateProperty.all<Color>(Color(0xffffffff)),
                shape: MaterialStateProperty.all(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                ),
              ),
              onPressed: () {
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(
                //     builder: (context) => EditMedicalHistory(student: widget.student),
                //   ),
                // );
              },
              child: Text('Medical history'),
            ),
          ),
          Container(
            width: 150, // Set your desired width here
            child: ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Color(0xff18588d)), // Set button background color
                foregroundColor: MaterialStateProperty.all<Color>(Color(0xffffffff)),
                shape: MaterialStateProperty.all(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                ),
              ),
              onPressed: () {
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(
                //     builder: (context) => EditAllergies(student: widget.student),
                //   ),
                // );
              },
              child: Text('Allergies'),
            ),
          ),
          Container(
            width: 150, // Set your desired width here
            child: ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Color(0xff18588d)), // Set button background color
                foregroundColor: MaterialStateProperty.all<Color>(Color(0xffffffff)),
                shape: MaterialStateProperty.all(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                ),
              ),
              onPressed: () {
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(
                //     builder: (context) => EditFamilyHistory(student: widget.student),
                //   ),
                // );
              },
              child: Text('Family history'),
            ),
          ),
          Container(
            width: 150, // Set your desired width here
            child: ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Color(0xff18588d)), // Set button background color
                foregroundColor: MaterialStateProperty.all<Color>(Color(0xffffffff)),
                shape: MaterialStateProperty.all(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                ),
              ),
              onPressed: () {
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(
                //     builder: (context) => EditChronicIllness(student: widget.student),
                //   ),
                // );
              },
              child: Text('Chronic Illness'),
            ),
          ),
          Container(
            width: 150, // Set your desired width here
            child: ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Color(0xff18588d)), // Set button background color
                foregroundColor: MaterialStateProperty.all<Color>(Color(0xffffffff)),
                shape: MaterialStateProperty.all(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                ),
              ),
              onPressed: () {
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(
                //     builder: (context) => EditChronicIllness(student: widget.student),
                //   ),
                // );
              },
              child: Text('Exams '),
            ),
          ),

        ],
      ),

      //   FAB 2 end

    );
  }

  String formatDateTime(DateTime dateTime) {
    // Create a DateFormat with the desired format
    final DateFormat formatter = DateFormat('yyyy-MM-dd HH:mm');
    // Return the formatted date and time string
    return formatter.format(dateTime);
  }

  int calculateAge(DateTime birthdate) {
    DateTime currentDate = DateTime.now();
    int age = currentDate.year - birthdate.year;
    if (birthdate.month > currentDate.month ||
        (birthdate.month == currentDate.month && birthdate.day > currentDate.day)) {
      age--;
    }
    return age;
  }
}
