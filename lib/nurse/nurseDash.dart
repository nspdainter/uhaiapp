import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:uhaiapp/login_prod.dart';
import 'package:uhaiapp/nurse/account_profile.dart';
import 'package:uhaiapp/nurse/settings_page.dart';
import 'package:uhaiapp/nurse/students.dart';
import 'package:uhaiapp/nurse/studts.dart';

import 'allStudents.dart';
import 'localStudents.dart';
import 'allExams.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NurseDash extends StatefulWidget {
  final int nurse_id;


   NurseDash({
    Key? key,
    required this.nurse_id,
  }) : super(key: key);

  @override
  _NurseDashState createState() => _NurseDashState();
}

class _NurseDashState extends State<NurseDash>{

   late int school_id;

  late Future<Map<String, dynamic>> _schoolDataFuture;

  final List<Map<String, dynamic>> gridItems = [
    {'title': 'Students', 'subtitle': '', 'icon': Icons.people_rounded, 'image': 'assets/images/students.png',},
    {'title': 'Medical Exams', 'subtitle': '', 'icon': Icons.medication_liquid, 'image': 'assets/images/exams.png',},
    // {'title': 'Water Analysis', 'subtitle': '', 'icon': Icons.water_drop_outlined, 'image': 'assets/images/water_icon.png',},
    // {'title': 'School Assesment Agenda256', 'subtitle': '9', 'icon': Icons.note_add_sharp, 'image': 'assets/images/questionnaire.png',},
    // {'title': 'Notifications', 'subtitle': '16', 'icon': Icons.notifications_active, 'image': 'assets/images/notification.png',},
    // {'title': 'Statistics', 'subtitle': '657', 'icon': Icons.stacked_bar_chart_outlined, 'image': 'assets/images/stats.png',},
    // {'title': 'Referrals', 'subtitle': '5', 'icon': Icons.account_balance, 'image': 'assets/images/referrals.png',},
    {'title': 'Settings', 'subtitle': '', 'icon': Icons.settings, 'image': 'assets/images/settings.png',},
    // {'title': 'Nurse Profile', 'subtitle': '', 'icon': Icons.person, 'image': 'assets/images/profile.png',},
    // Add more items as needed
  ];

  Future<Map<String, dynamic>> _getSchoolData(int userId) async {
    final Uri url = Uri.parse('https://uhaihealthcare.com/api/v1/school/school-users/user_id/${widget.nurse_id}');

    try {
      final http.Response response = await http.get(url);
      if (response.statusCode == 200) {
        final dynamic responseData = json.decode(response.body);
        if (responseData is Map<String, dynamic>) {
          // If responseData is already a Map, return it directly
          return responseData;
        } else if (responseData is List<dynamic> && responseData.isNotEmpty) {
          // If responseData is a List, return the first item assuming it's a Map
          return responseData.first;
        } else {
          throw Exception('Invalid response data format');
        }
      } else {
        throw Exception('Failed to load school data');
      }
    } catch (e) {
      throw Exception('Failed to fetch school data: $e');
    }
  }

  @override
  void initState() {
    super.initState();
    _schoolDataFuture = _getSchoolData(widget.nurse_id);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor:  const Color(0xff18588d),
          title: const Text('Nurse Dashboard',
            style: TextStyle(color: Color(0xffffffff)),
          ),
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: const Icon(Icons.menu),
                color:  const Color(0xffffffff),
                onPressed: () {
                  Scaffold.of(context).openDrawer();
                },
              );
            },
          ),
        ),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              const DrawerHeader(
                decoration: BoxDecoration(
                    color: Color(0xff18588d)
                ),
                child: Text(
                  'Menu',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 24,
                  ),
                ),
              ),
              ListTile(
                title: const Text('Home'),
                onTap: () {
                  // Handle drawer item click
                  Navigator.pop(context); // Close the drawer
                },
              ),
              ListTile(
                title: const Text('Students'),
                onTap: () {
                  // Handle drawer item click
                  Navigator.pop(context); // Close the drawer
                },
                // Add more drawer items as needed
              ),
              ListTile(
                title: const Text('Exams'),
                onTap: () {
                  // Handle drawer item click
                  Navigator.pop(context); // Close the drawer
                },
                // Add more drawer items as needed
              ),
              ListTile(
                title: const Text('Account details'),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => AccountDetailsPage(school_id: school_id)),
                  );// Close the drawer
                },
                // Add more drawer items as needed
              ),
              ListTile(
                title: const Text('Logout'),
                onTap: () {
                  // Handle drawer item click
                  _logout(context);
                },
                // Add more drawer items as needed
              ),
            ],
          ),
        ),
        body: Padding(
          padding: EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              // ///////////////
              FutureBuilder<Map<String, dynamic>>(
                future: _schoolDataFuture,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  } else if (snapshot.hasError) {
                    return Center(
                      child: Text('Error: ${snapshot.error}'),
                    );
                  } else {
                    final schoolData = snapshot.data!;
                    school_id = schoolData['school']['id'];
                    return
                      Container(
                        width: double.infinity,
                        child:  Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5.0), // Adjust the radius as needed
                          ),

                          color: const Color(0xffb3d1aa),
                          child: Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [

                                Text(
                                  '${schoolData['school']['name']}'.toUpperCase(),
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                SizedBox(height: 18,),

                                Text(
                                  "Nurse details".toUpperCase(),
                                  style: TextStyle(

                                    color: Colors.black,
                                    fontSize: 13,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                SizedBox(height: 5,),
                                Wrap(
                                  spacing: 10,
                                  runSpacing: 10,
                                  children: [
                                    Text('Names:',  style: TextStyle(
                                      color: Colors.black,
                                      // fontSize: 15,
                                      fontWeight: FontWeight.bold,
                                    ),),
                                    // Text(schoolData['user']['first_name'],  style: TextStyle(
                                    //   color: Colors.black,
                                    //   fontWeight: FontWeight.bold,
                                    // ),),
                                    // Text(schoolData['user']['last_name'],  style: TextStyle(
                                    //   color: Colors.black,
                                    //   fontWeight: FontWeight.bold,
                                    // ),),
                                  ],
                                ),
                                // SizedBox(height: 8),
                                Text(
                                  "Nurse ID: ${widget.nurse_id}",
                                  style: TextStyle(
                                    color: Colors.black,
                                    // fontSize: 15,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );

                  }
                },
              ),

              // ///////////////

              //   /////// quick info start

              //   ////// quick info end
              SizedBox(
                height: 30,
              ),
              //   ///// grid tiles start here
              Expanded(
                child: SingleChildScrollView(
                  child: GridView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 15.0,
                      mainAxisSpacing: 15.0,
                    ),
                    itemCount: gridItems.length,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () {
                          // Handle item click here

                          // click on students
                          if (index == 0) {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => StudentsPage(school_id: school_id)),
                            );
                          }
                          else if (index == 1) {
                            // Navigator.push(
                            //   context,
                            //   MaterialPageRoute(builder: (context) => const AllExams()),
                            // );
                          }
                          else if (index == 2) {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => SettingsPage()),
                            );
                          }
                          else {

                          }
                        },
                        child: Card(
                          // shape: RoundedRectangleBorder(
                          //   borderRadius: BorderRadius.zero, // Set to zero for no round corners
                          // ),
                          color: const Color(0xfff1f8ed),
                          elevation: 0.5,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                gridItems[index]['image'],
                                // Additional properties can be specified here
                                width: 50.0,
                                height: 50.0,
                                fit: BoxFit.contain, // or BoxFit.cover, BoxFit.fill, etc.
                              ),
                              // Icon(
                              //   gridItems[index]['icon'],
                              //   size: 30.0,
                              //   color: const Color(0xff18588d),
                              // ),
                              const SizedBox(height: 8.0),
                              Center(
                                child: Container(

                                  // alignment: Alignment.center,
                                  child: Text(
                                    gridItems[index]['title'],
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 14.0
                                    ),
                                  ),
                                ),

                              ),


                              const SizedBox(height: 8.0),
                              Text(
                                gridItems[index]['subtitle'],
                                style: const TextStyle(fontSize: 14.0),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                ),
              )
              //   //// grid tiles end here
            ],
          ),
        )





    );
  }
}




Future<void> _logout(BuildContext context) async {
  // Clear token from SharedPreferences
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.remove('token');

  // Navigate back to login page
  Navigator.of(context).push(
    MaterialPageRoute(
      builder: (context) => LoginProd(),
    ),
  );
}

TableRow _buildTableRow(String label, String value) {
  return TableRow(
    children: [
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 5.0),
        child: Text(
          label,
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 5.0),
        child: Text(
          value,
          style: TextStyle(
            fontSize: 16.0,
          ),
        ),
      ),
    ],
  );
}

