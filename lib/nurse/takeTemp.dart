import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:intl/intl.dart';



class TakeTemp extends StatefulWidget {
  const TakeTemp({super.key});

  @override
  _TakeTempState createState() => _TakeTempState();
}

class _TakeTempState extends State<TakeTemp> {
  List<Map<String, dynamic>> _records = [];

  @override
  void initState() {
    super.initState();
    _refreshRecords();
  }

  Future<void> _refreshRecords() async {
    final data = await DatabaseHelper.instance.queryAll();
    setState(() {
      _records = data;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: const Text('Temperature Recorder')),
        body: ListView.builder(
          itemCount: _records.length,
          itemBuilder: (context, index) {
            return ListTile(
              title: Text('${_records[index]['studentName']} - ${_records[index]['temperature']}°'),
              subtitle: Text('ID: ${_records[index]['studentId']} - Date: ${_records[index]['dateTime']}'),
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            await showDialog(
              context: context,
              builder: (context) => const TemperatureRecordForm(),
            );
            _refreshRecords();
          },
          child: const Icon(Icons.add),
        ),
      ),
    );
  }
}

class TemperatureRecordForm extends StatefulWidget {
  const TemperatureRecordForm({super.key});

  @override
  _TemperatureRecordFormState createState() => _TemperatureRecordFormState();
}

class _TemperatureRecordFormState extends State<TemperatureRecordForm> {
  final _formKey = GlobalKey<FormState>();
  String _studentName = '';
  String _studentId = '';
  String _temperature = '';

  Future<void> _saveRecord() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      final dateTime = DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now());
      await DatabaseHelper.instance.insertRecord({
        'studentName': _studentName,
        'studentId': _studentId,
        'temperature': _temperature,
        'dateTime': dateTime,
      });
      Navigator.of(context as BuildContext).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('Enter Temperature Record'),
      content: Form(
        key: _formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            TextFormField(
              decoration: const InputDecoration(labelText: 'Student Name'),
              onSaved: (value) => _studentName = value!,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter student name';
                }
                return null;
              },
            ),
            TextFormField(
              decoration: const InputDecoration(labelText: 'Student ID'),
              onSaved: (value) => _studentId = value!,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter student ID';
                }
                return null;
              },
            ),
            TextFormField(
              decoration: const InputDecoration(labelText: 'Temperature'),
              onSaved: (value) => _temperature = value!,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter temperature';
                }
                return null;
              },
            ),
          ],
        ),
      ),
      actions: <Widget>[
        TextButton(
          onPressed: () => Navigator.of(context).pop(),
          child: const Text('Cancel'),
        ),
        TextButton(
          onPressed: _saveRecord,
          child: const Text('Save'),
        ),
      ],
    );
  }
}

class DatabaseHelper {
  static const _databaseName = "TemperatureDatabase.db";
  static const _databaseVersion = 1;
  static const table = 'temperature_records';

  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  static Database? _database;
  Future<Database> get database async => _database ??= await _initDatabase();

  Future<Database> _initDatabase() async {
    String path = join(await getDatabasesPath(), _databaseName);
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
          CREATE TABLE $table (
            id INTEGER PRIMARY KEY,
            studentName TEXT NOT NULL,
            studentId TEXT NOT NULL,
            temperature TEXT NOT NULL,
            dateTime TEXT NOT NULL
          )
          ''');
  }

  Future<int> insertRecord(Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.insert(table, row);
  }

  Future<List<Map<String, dynamic>>> queryAll() async {
    Database db = await instance.database;
    return await db.query(table, orderBy: "dateTime DESC");
  }
}
