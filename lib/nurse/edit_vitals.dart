import 'dart:core';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';
import 'package:uhaiapp/nosql_models/student.dart';
import 'package:uhaiapp/nosql_models/astudent.dart';
import 'dart:convert';


import '../nosql_models/exam.dart';



// //////////

class EditRandomVitals extends StatefulWidget {
  final Astudent student;

  const EditRandomVitals({Key? key, required this.student}) : super(key: key);

  @override
  _EditRandomVitalsState createState() => _EditRandomVitalsState();
}

class _EditRandomVitalsState extends State<EditRandomVitals> {


  final _bloodGroupController = TextEditingController();
  final _tempController = TextEditingController();
  final _weightController = TextEditingController();
  final _heightController = TextEditingController();
  final  _spOxiController = TextEditingController();
  final _bpmOxiController = TextEditingController();
  final _glucoseController = TextEditingController();
  final _sysBPController = TextEditingController();
  final _diaBPController = TextEditingController();
  final _pulBPController = TextEditingController();
  final _drugsCommentController = TextEditingController();


  @override
  void initState() {
    super.initState();

    // _selectedSymptomsId = _selectedSymptoms.map((results) => results.id).toList();
    _tempController.addListener(_updateTempPreview);
    _heightController.addListener(_updateHeightPreview);
    _weightController.addListener(_updateWeightPreview);
    _glucoseController.addListener(_updateGlucosePreview);
    _sysBPController.addListener(_updateSYSBPPreview);
    _diaBPController.addListener(_updateDIABPPreview);
    _pulBPController.addListener(_updatePULBPPreview);
    _spOxiController.addListener(_updateSpoOxiPreview);
    _bpmOxiController.addListener(_updateBPMOxiPreview);

    //   oximeter values change of state
    int? getIntValue(String str) {
      try {
        final intValue = int.parse(str);
        return intValue;
      } catch (e) {
        return null; // Return null if parsing fails
      }
    }

  }

  void _updateTempPreview() {
    setState(() {
    });
  }
  void _updateWeightPreview() {
    setState(() {
    });
  }
  void _updateHeightPreview() {
    setState(() {
    });
  }

  void _updateGlucosePreview() {
    setState(() {
    });
  }

  void _updateSpoOxiPreview() {
    setState(() {
    });
  }

  void _updateBPMOxiPreview() {
    setState(() {
    });
  }

  void _updateSYSBPPreview() {
    setState(() {
    });
  }

  void _updateDIABPPreview() {
    setState(() {
    });
  }

  void _updatePULBPPreview() {
    setState(() {
    });
  }


  @override
  void dispose() {
    _bloodGroupController.removeListener(_updateTempPreview);
    _tempController.removeListener(_updateTempPreview);
    _heightController.removeListener(_updateTempPreview);
    _weightController.addListener(_updateWeightPreview);
    _glucoseController.removeListener(_updateTempPreview);
    _sysBPController.removeListener(_updateTempPreview);
    _diaBPController.removeListener(_updateTempPreview);
    _pulBPController.removeListener(_updateTempPreview);
    _spOxiController.removeListener(_updateTempPreview);
    _bpmOxiController.removeListener(_updateTempPreview);
    // Clean up the listener when the widget is disposed.

    _tempController.dispose();
    _heightController.dispose();
    _weightController.dispose();
    _glucoseController.dispose();
    _sysBPController.dispose();
    _diaBPController.dispose();
    _pulBPController.dispose();
    _spOxiController.dispose();
    _bpmOxiController.dispose();


    super.dispose();
  }

  int _currentStep = 0;



  String? _myOximeterValues;
  String? _myBloodPressureValues;
  String? _myRapidTests;


  String? _mySelectedSymptoms;
  // late List<DiseaseSymptom> _mySelectedSymptoms;

  String? _mySelectedDiseases;

  String? _mySelectedDrugs;
  // send data to db start

  final storeExamUrl = "https://copint.org/device_test/postExam.php";
  // ///// post method starts here

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xff18588d),
        title: Text("Conduct Medical Exam for ${widget.student.firstName}", style: const TextStyle(color: Color(0xffffffff))),
        iconTheme: const IconThemeData(
          color: Color(0xffffffff),
        ),
      ),
      body: SingleChildScrollView(
        // scrollDirection: Axis.vertical,
        child:
        Column(
          children: [
            SizedBox(height: 50,),
            Padding(
              padding: EdgeInsets.all(18.0),
              child: Wrap(
                alignment: WrapAlignment.center,
                spacing: 40.0,
                runSpacing: 40.0,
                children: [
                  SizedBox(
                      width: 180,
                      height: 40,
                      child:ElevatedButton.icon(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(Color(0xff18588d)), // Set button background color
                          foregroundColor: MaterialStateProperty.all<Color>(Color(0xffffffff)),
                          shape: MaterialStateProperty.all(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                          ),
                        ),
                        onPressed: () => _showBloodGroupForm(context),
                        icon: const Icon(Icons.bloodtype_outlined),
                        label: const Text('Blood Group'),
                      )),
                  SizedBox(
                      width: 180,
                      height: 40,
                      child:ElevatedButton.icon(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(Color(0xff18588d)), // Set button background color
                          foregroundColor: MaterialStateProperty.all<Color>(Color(0xffffffff)),
                          shape: MaterialStateProperty.all(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                          ),
                        ),
                        onPressed: () => _showTemperatureForm(context),
                        icon: const Icon(Icons.thermostat_sharp),
                        label: const Text('Temperature'),
                      )),
                  SizedBox(
                      width: 180,
                      height: 40,
                      child:ElevatedButton.icon(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(Color(0xff18588d)), // Set button background color
                          foregroundColor: MaterialStateProperty.all<Color>(Color(0xffffffff)),
                          shape: MaterialStateProperty.all(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                          ),
                        ),
                        onPressed: () => _showWeightForm(context),
                        icon: const Icon(Icons.monitor_weight_outlined),
                        label: const Text('Weight'),
                      )),
                  SizedBox(
                      width: 180,
                      height: 40,
                      child:ElevatedButton.icon(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(Color(0xff18588d)), // Set button background color
                          foregroundColor: MaterialStateProperty.all<Color>(Color(0xffffffff)),
                          shape: MaterialStateProperty.all(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                          ),
                        ),
                        onPressed: () => _showOximeterForm(context),
                        icon: const Icon(Icons.all_inclusive_rounded),
                        label: const Text('Oximeter'),
                      )),
                  SizedBox(
                      width: 180,
                      height: 40,
                      child:ElevatedButton.icon(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(Color(0xff18588d)), // Set button background color
                          foregroundColor: MaterialStateProperty.all<Color>(Color(0xffffffff)),
                          shape: MaterialStateProperty.all(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                          ),
                        ),
                        onPressed: () => _showBloodPressureForm(context),
                        icon: const Icon(Icons.bloodtype_outlined),
                        label: const Text('Blood pressure'),
                      )),
                  SizedBox(
                    width: 180,
                    height: 40,
                    child:ElevatedButton.icon(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(Color(0xff18588d)), // Set button background color
                        foregroundColor: MaterialStateProperty.all<Color>(Color(0xffffffff)),
                        shape: MaterialStateProperty.all(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                        ),
                      ),
                      onPressed: () => _showGlucoseForm(context),
                      icon: const Icon(Icons.electric_meter_outlined),
                      label: const Text('Glucose'),
                    ),
                  ),
                  SizedBox(
                    width: 180,
                    height: 40,
                    child: ElevatedButton.icon(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(Color(0xff18588d)), // Set button background color
                        foregroundColor: MaterialStateProperty.all<Color>(Color(0xffffffff)),
                        shape: MaterialStateProperty.all(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                        ),
                      ),
                      onPressed: () => _showHeightForm(context),
                      icon: const Icon(Icons.height),
                      label: const Text('Height'),
                    ),
                  ),

                ]
            ),
            )
          ],
        ),



      ),
    );
  }


//   ////////////////// vitals methods

  void _showBloodGroupForm(BuildContext context) {
    // Now show the dialog.
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder( // Use StatefulBuilder to update the dialog's content
          builder: (context, setState) {
            return AlertDialog(
              title: const Row(
                mainAxisSize: MainAxisSize.min,
                children: [

                  Expanded(child: Text('Blood Group')),
                  // CircularProgressIndicator(), // Reflect current state
                ],
              ),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[

                    const Text('Enter the Blood Group:'),
                    const SizedBox(height: 20,),
                    TextField(
                      // readOnly: true,
                      autofocus: true,
                      controller: _bloodGroupController, // Assuming this gets updated when scan finishes
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        // hintText: 'Temperature in °C',
                      ),
                    ),
                  ],
                ),
              ),
              actions: <Widget>[
                TextButton(
                  child: const Text('Cancel'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                TextButton(
                  child: const Text('Save'),
                  onPressed: () {
                    setState(() {
                      // This assumes _tempController.text contains the new temperature value.
                      // You might need to convert or validate the value as necessary.
                    });
                    // Logic to save the temperature here
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      },
    );
  }

  void _showTemperatureForm(BuildContext context) {
    // Now show the dialog.
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder( // Use StatefulBuilder to update the dialog's content
          builder: (context, setState) {
            return AlertDialog(
              title: const Row(
                mainAxisSize: MainAxisSize.min,
                children: [

                  Expanded(child: Text('Temperature')),
                  // CircularProgressIndicator(), // Reflect current state
                ],
              ),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[

                    const Text('Enter the temperature: (°C)'),
                    const SizedBox(height: 20,),
                    TextField(
                      // readOnly: true,
                      autofocus: true,
                      controller: _tempController, // Assuming this gets updated when scan finishes
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        // hintText: 'Temperature in °C',
                      ),
                    ),
                  ],
                ),
              ),
              actions: <Widget>[
                TextButton(
                  child: const Text('Cancel'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                TextButton(
                  child: const Text('Save'),
                  onPressed: () {
                    setState(() {
                      // This assumes _tempController.text contains the new temperature value.
                      // You might need to convert or validate the value as necessary.
                    });
                    // Logic to save the temperature here
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      },
    );
  }

  void _showOximeterForm(BuildContext context) {
    // Now show the dialog.
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder( // Use StatefulBuilder to update the dialog's content
          builder: (context, setState) {
            return AlertDialog(
              title: const Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  // /////////////////

                  // ////////////////
                  Expanded(child: Text('Oximetry')),

                ],
              ),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[

                    const Text('Enter values:'),
                    const SizedBox(height: 20,),
                    TextField(
                      // readOnly: true,
                      autofocus: true,
                      controller: _spOxiController, // Assuming this gets updated when scan finishes
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Sp02',
                      ),
                    ),
                    const SizedBox(height: 10),
                    TextField(
                      // readOnly: true,
                      autofocus: true,
                      controller: _bpmOxiController, // Assuming this gets updated when scan finishes
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'PRbpm',
                      ),
                    ),
                  ],
                ),
              ),
              actions: <Widget>[
                TextButton(
                  child: const Text('Cancel'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                TextButton(
                  child: const Text('Save'),
                  onPressed: () {
                    // setState(() {
                    //   int? getIntValue(String str) {
                    //     try {
                    //       final intValue = int.parse(str);
                    //       return intValue;
                    //     } catch (e) {
                    //       return null; // Return null if parsing fails
                    //     }
                    //   }
                    //   List<Map<String, int?>> theOximeterValues = [ {"sp02":getIntValue(_spOxiController.text), "prbpm":getIntValue(_bpmOxiController.text)}];
                    //   String oximeterToJson(List<Map<String, int?>> theOximeterValues) => json.encode(List<dynamic>.from(theOximeterValues.map((x) => Map.from(x).map((k, v) => MapEntry<String, dynamic>(k, v)))));
                    //   final oximeterValues = oximeterToJson(theOximeterValues);
                    //   _myOximeterValues = oximeterValues;
                    //   print(_myOximeterValues);
                    // });
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      },
    );
  }

  void _showWeightForm(BuildContext context) {
    // Now show the dialog.
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder( // Use StatefulBuilder to update the dialog's content
          builder: (context, setState) {
            return AlertDialog(
              title: const Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  // /////////////////

                  // ////////////////
                  Expanded(child: Text('Weight')),
                  // CircularProgressIndicator(), // Reflect current state
                ],
              ),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[

                    const Text('Enter weight:'),
                    const SizedBox(height: 20,),
                    TextField(
                      // readOnly: true,
                      autofocus: true,
                      controller: _weightController, // Assuming this gets updated when scan finishes
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        // hintText: 'Temperature in °C',
                      ),
                    ),
                  ],
                ),
              ),
              actions: <Widget>[
                TextButton(
                  child: const Text('Cancel'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                TextButton(
                  child: const Text('Save'),
                  onPressed: () {
                    // Logic to save the temperature here
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      },
    );
  }

  void _showBloodPressureForm(BuildContext context) {
    // Now show the dialog.
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder( // Use StatefulBuilder to update the dialog's content
          builder: (context, setState) {
            return AlertDialog(
              title: const Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  // /////////////////

                  // ////////////////
                  Expanded(child: Text('Blood Pressure')),
                  // CircularProgressIndicator(), // Reflect current state
                ],
              ),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[

                    const Text('Enter values:'),
                    const SizedBox(height: 5,),
                    TextField(
                      // readOnly: true,
                      autofocus: true,
                      controller: _sysBPController, // Assuming this gets updated when scan finishes
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'SYS (mmHg)',
                      ),
                    ),
                    const SizedBox(height: 5,),
                    TextField(
                      // readOnly: true,
                      autofocus: true,
                      controller: _diaBPController, // Assuming this gets updated when scan finishes
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'DIA (mmHg)',
                      ),
                    ),
                    const SizedBox(height: 5,),
                    TextField(
                      // readOnly: true,
                      autofocus: true,
                      controller: _pulBPController, // Assuming this gets updated when scan finishes
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'PUL (/min)',
                      ),
                    ),
                  ],
                ),
              ),
              actions: <Widget>[
                TextButton(
                  child: const Text('Cancel'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                TextButton(
                  child: const Text('Save'),
                  onPressed: () {
                    // setState(() {
                    //   int? getIntValue(String str) {
                    //     try {
                    //       final intValue = int.parse(str);
                    //       return intValue;
                    //     } catch (e) {
                    //       return null; // Return null if parsing fails
                    //     }
                    //   }
                    //   List<Map<String, int?>> theBloodPressureValues = [ {"sys":getIntValue(_sysBPController.text), "dia":getIntValue(_diaBPController.text), "pul":getIntValue(_pulBPController.text)}];
                    //   String bloodPressureToJson(List<Map<String, int?>> theBloodPressureValues) => json.encode(List<dynamic>.from(theBloodPressureValues.map((x) => Map.from(x).map((k, v) => MapEntry<String, dynamic>(k, v)))));
                    //   final bloodPressureValues = bloodPressureToJson(theBloodPressureValues);
                    //   _myBloodPressureValues = bloodPressureValues;
                    //   // print(_myOximeterValues);
                    // });
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      },
    );
  }

  void _showHeightForm(BuildContext context) {
    // Now show the dialog.
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder( // Use StatefulBuilder to update the dialog's content
          builder: (context, setState) {
            return AlertDialog(
              title: const Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  // /////////////////

                  // ////////////////
                  Expanded(child: Text('Height')),
                  // CircularProgressIndicator(), // Reflect current state
                ],
              ),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[

                    const Text('Enter height: (cm)'),
                    const SizedBox(height: 20,),
                    TextField(
                      // readOnly: true,
                      autofocus: true,
                      controller: _heightController, // Assuming this gets updated when scan finishes
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        // hintText: 'Temperature in °C',
                      ),
                    ),
                  ],
                ),
              ),
              actions: <Widget>[
                TextButton(
                  child: const Text('Cancel'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                TextButton(
                  child: const Text('Save'),
                  onPressed: () {
                    // Logic to save the temperature here
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      },
    );
  }

  void _showGlucoseForm(BuildContext context) {
    // Now show the dialog.
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder( // Use StatefulBuilder to update the dialog's content
          builder: (context, setState) {
            return AlertDialog(
              title: const Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  // /////////////////

                  // ////////////////
                  Expanded(child: Text('Glucose Levels')),
                  // CircularProgressIndicator(), // Reflect current state
                ],
              ),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[

                    const Text('Enter level'),
                    const SizedBox(height: 20,),
                    TextField(
                      // readOnly: true,
                      autofocus: true,
                      controller: _glucoseController, // Assuming this gets updated when scan finishes
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'mg/dL',
                      ),
                    ),
                  ],
                ),
              ),
              actions: <Widget>[
                TextButton(
                  child: const Text('Cancel'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                TextButton(
                  child: const Text('Save'),
                  onPressed: () {
                    // Logic to save the temperature here
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      },
    );
  }

}
