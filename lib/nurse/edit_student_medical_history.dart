import 'dart:convert';
import 'dart:core';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:multi_select_flutter/multi_select_flutter.dart';
import 'allStudents.dart';


class MedicalHistory {
  int id;
  String condition_name;

  MedicalHistory({
    required this.id,
    required this.condition_name,
  });

  factory MedicalHistory.fromJson(Map<String, dynamic> json) {
    return MedicalHistory(
      id: json['condition'],
      condition_name: json['condition_name'] is String ? json['condition_name'] : 'Unknown',
    );
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'condition_name': condition_name,
  };
}

class EditStudMedicalHistory extends StatefulWidget {
  final Student student;

  const EditStudMedicalHistory({Key? key, required this.student}) : super(key: key);

  @override
  _EditStudMedicalHistoryState createState() => _EditStudMedicalHistoryState();
}

class _EditStudMedicalHistoryState extends State<EditStudMedicalHistory> {
  final _medicalHistoryCommentController = TextEditingController();

  List<MedicalHistory> _medicalHistory = [];
  List<MedicalHistory> _selectedMedicalHistory = [];
  List<MedicalHistory>? _mySelectedMedicalHistory;
  List<Map<String, String>> transformedList = [];

  @override
  void initState() {
    super.initState();
    _fetchMedicalHistory();
  }

  Future<void> _fetchMedicalHistory() async {
    try {
      final response = await http.get(Uri.parse('https://uhaihealthcare.com/api/v1/school/medical-conditions/condition/4/'));
      if (response.statusCode == 200) {
        List<dynamic> data = json.decode(response.body);
        setState(() {
          _medicalHistory = data.map((json) => MedicalHistory.fromJson(json)).toList();
        });
      } else {
        throw Exception('Failed to load medical history');
      }
    } catch (e) {
      print('Failed to load medical history: $e');
    }
  }

  @override
  void dispose() {
    _medicalHistoryCommentController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xff18588d),
        title: Text("${widget.student.first_name}", style: const TextStyle(color: Color(0xffffffff))),
        iconTheme: const IconThemeData(
          color: Color(0xffffffff),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(16.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Edit / Add Medical History", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.all(16.0),
              child: _medicalHistory.isEmpty
                  ? Center(child: CircularProgressIndicator())
                  : MultiSelectDialogField(
                searchable: true,
                items: _medicalHistory.map((medicalHistory) => MultiSelectItem<MedicalHistory>(medicalHistory, medicalHistory.condition_name)).toList(),
                title: const Text("Medical History"),
                selectedColor: Theme.of(context).primaryColor,
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor.withOpacity(0.1),
                  border: Border.all(
                    color: Theme.of(context).primaryColor,
                    width: 2,
                  ),
                ),
                buttonIcon: Icon(
                  Icons.add,
                  color: Theme.of(context).primaryColor,
                ),
                buttonText: const Text(
                  "Select Medical History",
                  style: TextStyle(
                    color: Color(0xff000000),
                    fontSize: 16,
                  ),
                ),
                onConfirm: (results) {
                  setState(() {
                    _selectedMedicalHistory = List<MedicalHistory>.from(results);
                    _mySelectedMedicalHistory = _selectedMedicalHistory;
                    transformedList = transformMedicalHistoryList(_mySelectedMedicalHistory);

                  });
                },
                chipDisplay: MultiSelectChipDisplay(
                  chipColor: const Color(0xffc5c5c5),
                  textStyle: const TextStyle(
                    color: Color(0xff000000),
                  ),
                  onTap: (value) {
                    setState(() {
                      _selectedMedicalHistory.remove(value);
                    });
                  },
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(16.0),
              child: TextField(
                controller: _medicalHistoryCommentController,
                maxLines: 5,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Add comments about the medical history',
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(16.0),
              child: ElevatedButton(
                onPressed: () {
                  saveMedicalHistory(context);
                },
                style: ElevatedButton.styleFrom(
                  foregroundColor: const Color(0xffffffff),
                  backgroundColor: const Color(0xff18588d),
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.zero,
                  ),
                ),
                child: const Text('Save medical history'),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> saveMedicalHistory(BuildContext context) async {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                CircularProgressIndicator(),
                SizedBox(width: 20),
                Text("Saving data..."),
              ],
            ),
          ),
        );
      },
    );

    Map<String, dynamic> medical_history_data = {
      "student_id": widget.student.id,
      'condition_type': 4,
      'selected_conditions': transformedList,
      'condition_comment': _medicalHistoryCommentController.text,
    };

    String encodedData = jsonEncode(medical_history_data);
    print(encodedData);

    // try catch starts here

    try {
      final response = await http.post(
        Uri.parse('https://uhaihealthcare.com/api/v1/school/add-student-medical-conditions/'),
        headers: {
          'Content-Type': 'application/json',
        },
        body: encodedData,
      );
      Navigator.pop(context);
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Response', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xff18588d))),
            content: Text(jsonDecode(response.body)['message'] ?? '', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xff008000))),
            actions: <Widget>[
              TextButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    } catch (e) {
      Navigator.pop(context);

      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Error', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xff000000))),
            content: Text('An error occurred: $e', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xffff0000))),
            actions: <Widget>[
              TextButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
    //   try catch ends here

  }
}

List<Map<String, String>> transformMedicalHistoryList(List<MedicalHistory>? medicalHistoryList) {
  if (medicalHistoryList == null) {
    return [];
  }
  return medicalHistoryList.map((illness) {
    return {"condition_name": illness.id.toString()};

  }).toList();
}
