import 'package:flutter/material.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';
import 'allStudents.dart';


// ////



// ////////////

class DiseaseSymptom {
  int id;
  String symptomName;

  DiseaseSymptom({
    required this.id,
    required this.symptomName,
  });

  factory DiseaseSymptom.fromJson(Map<String, dynamic> json) {
    return DiseaseSymptom(
      id: json['id'] is int ? json['id'] : 0,
      symptomName: json['symptomName'] is String ? json['symptomName'] : 'Unknown',
    );
  }
}


// //////////

class NewExam extends StatefulWidget {
  final Student student;

  const NewExam({Key? key, required this.student}) : super(key: key);

  @override
  _NewExamState createState() => _NewExamState();
}

class _NewExamState extends State<NewExam> {

  final _typeController = TextEditingController();
  final _commentsController = TextEditingController();
  final _tempController = TextEditingController();
  final _weightController = TextEditingController();
  final _oximeterController = TextEditingController();
  final _spirometerController = TextEditingController();
  final _prescriptionController = TextEditingController();

  int _currentStep = 0;



  final List<DiseaseSymptom> _symptoms = [
    DiseaseSymptom(id: 1, symptomName: "Fever"),
    DiseaseSymptom(id: 2, symptomName: "Cough"),
    DiseaseSymptom(id: 3, symptomName: "Fatigue"),
    DiseaseSymptom(id: 4, symptomName: "Nausea"),
    DiseaseSymptom(id: 5, symptomName: "Headache"),
    DiseaseSymptom(id: 6, symptomName: "Dizziness"),
    DiseaseSymptom(id: 7, symptomName: "Shortness of breath"),
    DiseaseSymptom(id: 8, symptomName: "Chest pain"),
    DiseaseSymptom(id: 9, symptomName: "Loss of taste or smell"),
    DiseaseSymptom(id: 10, symptomName: "Muscle or joint pain"),
    DiseaseSymptom(id: 11, symptomName: "Fever"),
    DiseaseSymptom(id: 12, symptomName: "Cough"),
    DiseaseSymptom(id: 13, symptomName: "Fatigue"),
    DiseaseSymptom(id: 14, symptomName: "Nausea"),
    DiseaseSymptom(id: 15, symptomName: "Headache"),
    DiseaseSymptom(id: 16, symptomName: "Dizziness"),
    DiseaseSymptom(id: 17, symptomName: "Shortness of breath"),
    DiseaseSymptom(id: 18, symptomName: "Chest pain"),
    DiseaseSymptom(id: 19, symptomName: "Loss of taste or smell"),
    DiseaseSymptom(id: 20, symptomName: "Muscle or joint pain"),
    // Add your symptoms here...
  ];

  // To store the selected symptoms
  List<DiseaseSymptom> _selectedSymptoms = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xff18588d),
        title: Text("New Exam ${widget.student.first_name}"),
        iconTheme: const IconThemeData(
            color: Color(0xffffffff),
        ),
      ),
      body: SingleChildScrollView(
        child: Stepper(
          steps: getSteps(),
          onStepCancel: () {
            if(_currentStep > 0){
              setState(() {
                _currentStep--;
              });
            }

          },
          currentStep: _currentStep,
          onStepContinue: () {
            final isLastStep = _currentStep == getSteps().length -1;
            if(isLastStep){
              showDialog(
                  context: context,
                  builder: (BuildContext context){
                    return AlertDialog(
                      title: const Text('Exam Submitted'),
                      content: const Text('The exam has been submitted successfully'),
                      actions: [
                        TextButton(
                            onPressed: (){
                              Navigator.pop(context);

                              }, child: const Text('OK'))
                      ],
                    );

              });
            }
            else {
              setState(() {
                _currentStep++;
              });
            }
          },

        ),

      ),
    );
  }

  List<Step> getSteps(){
    return <Step> [
      Step(
          title: const Text('Symptoms & General comments'),
          content: Column(
            children: [
             MultiSelectDialogField(
                    items: _symptoms.map((symptom) => MultiSelectItem<DiseaseSymptom>(symptom, symptom.symptomName)).toList(),
                    title: const Text("Symptoms"),
                    selectedColor: Theme.of(context).primaryColor,
                    decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor.withOpacity(0.1),
                      // borderRadius: BorderRadius.all(Radius.circular(0)),
                      border: Border.all(
                        color: Theme.of(context).primaryColor,
                        width: 2,
                      ),
                    ),
                    buttonIcon: Icon(
                      Icons.add,
                      color: Theme.of(context).primaryColor,
                    ),
                    buttonText: const Text(
                      "Select Symptoms",
                      style: TextStyle(
                        color: Color(0xff000000),
                        fontSize: 16,
                      ),
                    ),
                    onConfirm: (results) {
                      _selectedSymptoms = List<DiseaseSymptom>.from(results);
                    },
                    chipDisplay: MultiSelectChipDisplay(
                      chipColor: const Color(0xffc5c5c5),
                      textStyle: const TextStyle(
                        color: Color(0xff000000),
                      ),
                      onTap: (value) {
                        setState(() {
                          _selectedSymptoms.remove(value);
                        });
                      },
                    ),
                  ),
             const Padding(padding: EdgeInsets.all(10)),
             const TextField(
            maxLines: 3,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Comments'
            ),
             ),
            ],
          ),),
      const Step(
          title: Text('Categorise the examination'),
          content: Column(
            children: [

            ],
          )),
      Step(
        title: const Text('Take Vitals'),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // Padding(padding: EdgeInsets.all(10)),

              const Text('Temperature'),

              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Row(
                  children: <Widget>[
                    const Expanded(
                      // Use Expanded to ensure the TextField occupies the remaining space
                      child: TextField(
                        // controller: _controller,
                        decoration: InputDecoration(
                          hintText: 'Temperature',
                          border: OutlineInputBorder(),
                        ),
                      ),
                    ),
                    IconButton(
                      icon: const Icon(Icons.edit),
                      onPressed: () {
                        // Implement edit functionality
                        print('Edit action');
                      },
                    ),
                    IconButton(
                      icon: const Icon(Icons.save),
                      onPressed: () {
                        // Implement save functionality
                        print('Save action with text: ');
                      },
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 30,),
              const Text('Oximeter values'),
              const Padding(
                padding: EdgeInsets.all(20.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      // Use Expanded to ensure the TextField occupies the remaining space
                      child: TextField(
                        // controller: _controller,
                        decoration: InputDecoration(
                          hintText: 'Sp02',
                          border: OutlineInputBorder(),
                        ),
                      ),
                    ),
                    // IconButton(
                    //   icon: Icon(Icons.edit),
                    //   onPressed: () {
                    //     // Implement edit functionality
                    //     print('Edit action');
                    //   },
                    // ),
                    // IconButton(
                    //   icon: Icon(Icons.save),
                    //   onPressed: () {
                    //     // Implement save functionality
                    //     print('Save action with text: ');
                    //   },
                    // ),
                  ],
                ),
              ),
              const Padding(
                padding: EdgeInsets.all(20.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      // Use Expanded to ensure the TextField occupies the remaining space
                      child: TextField(
                        // controller: _controller,
                        decoration: InputDecoration(
                          hintText: 'PR bpm',
                          border: OutlineInputBorder(),
                        ),
                      ),
                    ),
                    // IconButton(
                    //   icon: Icon(Icons.edit),
                    //   onPressed: () {
                    //     // Implement edit functionality
                    //     print('Edit action');
                    //   },
                    // ),
                    // IconButton(
                    //   icon: Icon(Icons.save),
                    //   onPressed: () {
                    //     // Implement save functionality
                    //     print('Save action with text: ');
                    //   },
                    // ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Row(
                  children: <Widget>[

                    IconButton(
                      icon: const Icon(Icons.edit),
                      onPressed: () {
                        // Implement edit functionality
                        print('Edit action');
                      },
                    ),
                    IconButton(
                      icon: const Icon(Icons.save),
                      onPressed: () {
                        // Implement save functionality
                        print('Save action with text: ');
                      },
                    ),
                  ],
                ),
              ),
            ],
          )
      ),
      Step(
          title: const Text('Prescriptions'),
          content: Column(
            children: [
              MultiSelectDialogField(
                items: _symptoms.map((symptom) => MultiSelectItem<DiseaseSymptom>(symptom, symptom.symptomName)).toList(),
                title: const Text("Symptoms"),
                selectedColor: Theme.of(context).primaryColor,
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor.withOpacity(0.1),
                  // borderRadius: BorderRadius.all(Radius.circular(0)),
                  border: Border.all(
                    color: Theme.of(context).primaryColor,
                    width: 2,
                  ),
                ),
                buttonIcon: Icon(
                  Icons.add,
                  color: Theme.of(context).primaryColor,
                ),
                buttonText: const Text(
                  "Select Symptoms",
                  style: TextStyle(
                    color: Color(0xff000000),
                    fontSize: 16,
                  ),
                ),
                onConfirm: (results) {
                  _selectedSymptoms = List<DiseaseSymptom>.from(results);
                },
                chipDisplay: MultiSelectChipDisplay(
                  chipColor: const Color(0xffc5c5c5),
                  textStyle: const TextStyle(
                    color: Color(0xff000000),
                  ),
                  onTap: (value) {
                    setState(() {
                      _selectedSymptoms.remove(value);
                    });
                  },
                ),
              ),
              const Padding(padding: EdgeInsets.all(10)),
              const TextField(
                // maxLines: 5,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Add prescriptiion'
                ),
              ),
            ],
          )
      ),

      Step(
          title: const Text('Make referrral'),
          content: Column(
            children: [
              MultiSelectDialogField(
                items: _symptoms.map((symptom) => MultiSelectItem<DiseaseSymptom>(symptom, symptom.symptomName)).toList(),
                title: const Text("Symptoms"),
                selectedColor: Theme.of(context).primaryColor,
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor.withOpacity(0.1),
                  // borderRadius: BorderRadius.all(Radius.circular(0)),
                  border: Border.all(
                    color: Theme.of(context).primaryColor,
                    width: 2,
                  ),
                ),
                buttonIcon: Icon(
                  Icons.add,
                  color: Theme.of(context).primaryColor,
                ),
                buttonText: const Text(
                  "Select Symptoms",
                  style: TextStyle(
                    color: Color(0xff000000),
                    fontSize: 16,
                  ),
                ),
                onConfirm: (results) {
                  _selectedSymptoms = List<DiseaseSymptom>.from(results);
                },
                chipDisplay: MultiSelectChipDisplay(
                  chipColor: const Color(0xffc5c5c5),
                  textStyle: const TextStyle(
                    color: Color(0xff000000),
                  ),
                  onTap: (value) {
                    setState(() {
                      _selectedSymptoms.remove(value);
                    });
                  },
                ),
              ),
              const Padding(padding: EdgeInsets.all(10)),

            ],
          )
      ),



    ];
  }

}
