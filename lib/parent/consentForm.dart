import 'package:flutter/material.dart';
import 'dart:convert';

void main() => runApp(const MaterialApp(home: ConsentForm()));

class ConsentForm extends StatefulWidget {
  const ConsentForm({super.key});

  @override
  _ConsentFormState createState() => _ConsentFormState();
}

class _ConsentFormState extends State<ConsentForm> {
  bool _isReadButNotUnderstood = false;
  bool _isReadAndUnderstood = false;
  String? _radioValue; // For the radio buttons
  bool _contactByEmail = false;
  bool _contactBySMS = false;
  bool _contactByPhoneCall = false;

  void _showSelectionDialog() {
    var selectedData = {
      'readAndUnderstood': _isReadAndUnderstood,
      'readButNotUnderstood': _isReadButNotUnderstood,
      'diagnosisConsent': _radioValue,
      'contactByEmail': _contactByEmail,
      'contactBySMS': _contactBySMS,
      'contactByPhoneCall': _contactByPhoneCall,
    };

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text("Confirm Your Selection"),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(jsonEncode(selectedData, toEncodable: (dynamic item) => item.toString())),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: const Text('Submit'),
              onPressed: () {
                // Implement the submit functionality here
                Navigator.of(context).pop();
                print(jsonEncode(selectedData));
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xff18588d),
        title: const Text('Consent Form', style: TextStyle(color: Colors.white)),
        iconTheme: const IconThemeData(color: Colors.white),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView( // Added to ensure the form is scrollable
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 30),
              // Question One with Checkbox options
              const Text(
                '1. Have you read and understood our terms and conditions?',
                style: TextStyle(fontSize: 16),
              ),
              CheckboxListTile(
                title: const Text('Yes, I have read but NOT understood.'),
                value: _isReadButNotUnderstood,
                onChanged: (bool? value) {
                  setState(() {
                    _isReadButNotUnderstood = value!;
                  });
                },
              ),
              CheckboxListTile(
                title: const Text('Yes, I have read and understood.'),
                value: _isReadAndUnderstood,
                onChanged: (bool? value) {
                  setState(() {
                    _isReadAndUnderstood = value!;
                  });
                },
              ),
              const Divider(),

              // Question Two with Radio button options
              const Text(
                '2. Do you wish your child to be part of the School Digital Health Program:',
                style: TextStyle(fontSize: 16),
              ),
              RadioListTile<String>(
                title: const Text('Yes'),
                value: 'Yes',
                groupValue: _radioValue,
                onChanged: (String? value) {
                  setState(() {
                    _radioValue = value;
                  });
                },
              ),
              RadioListTile<String>(
                title: const Text('No'),
                value: 'No',
                groupValue: _radioValue,
                onChanged: (String? value) {
                  setState(() {
                    _radioValue = value;
                  });
                },
              ),
              const Divider(),

              // Question Three with Checkbox options
              const Text(
                '3. How do you wish to be contacted for medical intervetions concerning your child?',
                style: TextStyle(fontSize: 16),
              ),
              CheckboxListTile(
                title: const Text('Email'),
                value: _contactByEmail,
                onChanged: (bool? value) {
                  setState(() {
                    _contactByEmail = value!;
                  });
                },
              ),
              CheckboxListTile(
                title: const Text('SMS'),
                value: _contactBySMS,
                onChanged: (bool? value) {
                  setState(() {
                    _contactBySMS = value!;
                  });
                },
              ),
              CheckboxListTile(
                title: const Text('Phone call'),
                value: _contactByPhoneCall,
                onChanged: (bool? value) {
                  setState(() {
                    _contactByPhoneCall = value!;
                  });
                },
              ),
              const Divider(),

              // Save button
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 20.0),
                child: Center(
                  child: ElevatedButton(
                    onPressed: _showSelectionDialog,
                    style: ElevatedButton.styleFrom(
                      backgroundColor: const Color(0xff18588d),
                      minimumSize: const Size(200, 50),
                      shape: const RoundedRectangleBorder(borderRadius: BorderRadius.zero),
                    ),
                    child: const Text('Save', style: TextStyle(color: Colors.white, fontSize: 16)),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
